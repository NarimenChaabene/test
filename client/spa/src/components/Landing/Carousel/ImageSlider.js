import React from 'react'
import {Carousel} from 'react-bootstrap'
import Button from '@material-ui/core/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import image1 from '../../../images/img-1.jpg'
import image2 from '../../../images/img-2.jpg'
import { Link } from 'react-router-dom';


import './ImageSlider.css'



function ImageSlider () {
    return (
        <div>
          
  <Carousel className="carousel"
  autoPlay 
  interval={5000000} 
  infiniteLoop
  thumbWidth={120} 
  showIndicators={false}
  showStatus={false}>
    
    
  <Carousel.Item className="carousel-item" >
    <img
      className="block1"
      src={image1}
      alt="First slide"
    />
    <Carousel.Caption className="slide1">
    <div className="img1">
    <h2 className="h2"> BIENVENU  AU SPATHERME</h2>
      <p className="text-relax">Détendez votre corps et votre âme</p>
      </div> 
    <div className="btn-image">
        <button className="btn1"> Préinscription</button>
    </div>
    </Carousel.Caption>
  </Carousel.Item>

  

  <Carousel.Item className="carousel-item" >
    <img
      className="block1"
      src={image2}
      alt="Second slide"
    />
    <Carousel.Caption className="slide2">


    <div className="img2">
        <h2 className="h">BIENVENU AU SPATHERME</h2>
        <p className="p2">Détendez votre corps et votre âme</p>
    </div>


    <div className="btn-image">
      <Link to ='/PreInscription'>
        <button className="btn2"> Préinscription</button>
      </Link>
    </div>
    </Carousel.Caption>
  </Carousel.Item>


  
</Carousel>

        </div>

    )
}

export default ImageSlider;