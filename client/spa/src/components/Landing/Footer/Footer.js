import React from 'react'
import './Footer.css'


function Footer () {
    return(

        <div className="Footer-container">
            
            <div className="icons">
            <div className="FooterLink" href="#">
              <i className="fab fa-facebook-f icon-footer">
                
              </i>
            </div>

            <div className="FooterLink" href="#">
              <i className="fab fa-instagram  icon-footer">
                
              </i>
            </div>


            <div className="FooterLink" href="#">
              <i className="fab fa-youtube  icon-footer">
                
              </i>
            </div>
        </div>

            <div className="copyright">
            <span className="text-copyright">
                &copy;{new Date().getFullYear() } spathermes; All Rights Reserved 
            </span>
            </div>
            


            

        </div>

    )
}

export default Footer