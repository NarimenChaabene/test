import React from 'react'
import team1 from '../../../images/team1.jpg'
import team2 from '../../../images/team2.jpg'
import team3 from '../../../images/team3.jpg'

import './Therapist.css'

function Therapistes () {
    
        return(
    <div>
       <div className="therapiste"> <h4 className="text-therapist"> THÉRAPEUTE SPA THERME</h4> </div>
        
        <div className="items-container-therapist">
            <div className="card-container"> 
                <div className="item-therapist">
                    <img className="image-icons-therapist" src={team1} />

                        <div className="name">
                        <span className="caption-therapist" >BRIANA RACHEL</span>
                        </div>
                
                        <div className="work">
                        <span className="title-therapist" >Meditation Trainer</span>
                        </div>
                        <div className="icon-Link"  href="#">
                            <i className="fab fa-facebook-f black"></i>
                            <i class="fab fa-linkedin black"></i>
                            <i class="fab fa-instagram black"></i>
                        </div>
        
                </div>
            </div>




        

        <div className="card-container"> 
                <div className="item-therapist">
                    <img className="image-icons-therapist" src={team2} />

                        <div className="name">
                        <span className="caption-therapist" >BRIANA RACHEL</span>
                        </div>
                
                        <div className="work">
                        <span className="title-therapist" >Meditation Trainer</span>
                        </div>
                        <div className="icon-Link"  href="#">
                            <i className="fab fa-facebook-f black"></i>
                            <i class="fab fa-linkedin black"></i>
                            <i class="fab fa-instagram black"></i>
                        </div>
        
                </div>
            </div>




            <div className="card-container"> 
                <div className="item-therapist">
                    <img className="image-icons-therapist" src={team3} />

                        <div className="name">
                        <span className="caption-therapist" >BRIANA RACHEL</span>
                        </div>
                
                        <div className="work">
                        <span className="title-therapist" >Meditation Trainer</span>
                        </div>
                        <div className="icon-Link"  href="#">
                            <i className="fab fa-facebook-f black"></i>
                            <i class="fab fa-linkedin black"></i>
                            <i class="fab fa-instagram black"></i>
                        </div>
        
                </div>
            </div>

        </div>


            




        </div>

                 
                   

    
    

   
        )
    

    
}

export default Therapistes;