import React from 'react'
import './Gallery.css'

import gallery1 from '../../../images/gallery1.jfif'
import gallery2 from '../../../images/gallery2.jfif'
import gallery3 from '../../../images/gallery3.jfif'
import gallery4 from '../../../images/gallery4.jfif'
import gallery5 from '../../../images/gallery5.jfif'
import gallery6 from '../../../images/gallery6.jfif'
import gallery7 from '../../../images/gallery7.jfif'
import gallery8 from '../../../images/gallery8.jfif'
import gallery9 from '../../../images/gallery9.jfif'
import gallery10 from '../../../images/gallery10.jfif'



function Gallery () {

    return(
        <div>
            <div className="gallery-container">
                <h1 className="heading"> GALERIE SPA THERME</h1>
            </div>
            <div className="gallery">
            <div className="gallery-item">
                <img className="gallery-image" src={gallery1} />
            </div>

            <div className="gallery-item">
                <img className="gallery-image" src={gallery2} />
            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery3} />
            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery4} />
            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery5} />
            </div>

            <div className="gallery-item">
                <img className="gallery-image" src={gallery6} />

            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery7} />
            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery8} />
            </div>

            <div className="gallery-item">
                <img className="gallery-image" src={gallery9} />
            </div>


            <div className="gallery-item">
                <img className="gallery-image" src={gallery10} />
            </div>
        </div>
       



            

        </div>
       

        

    )
}


export default Gallery;