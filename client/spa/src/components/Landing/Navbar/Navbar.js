import React , {useState} from 'react'
import {Button} from './Button'
import {Link} from 'react-router-dom'
import './Navbar.css'
import Dropdown from './Dropdown'
import logo from '../../../images/logo.png'
import shopping from '../../../images/shopping.png'



function Navbar() {

  const navStyle={
    color:'black'
  }
    const [click, setClick] = useState(false);
    const [dropdown, setDropdown] = useState(false);
  
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
  
    const onMouseEnter = () => {
      if (window.innerWidth < 960) {
        setDropdown(false);
      } else {
        setDropdown(true);
      }
    };
  
    const onMouseLeave = () => {
      if (window.innerWidth < 960) {
        setDropdown(false);
      } else {
        setDropdown(false);
      }
    };


    return (
        <>
        <nav className='navbar'>
        <Link to='/'className='navbar-logo' onClick={closeMobileMenu}>
            <img className="logo" src={logo} />
            </Link>

            <div className='menu-icon' onClick={handleClick}>
          <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
        </div>

        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
          <li className='nav-item'>
            <Link to='/'style={navStyle} className='nav-links' onClick={closeMobileMenu}>
              ACCUEIL
            </Link>
          </li>
          <li
            className='nav-item'
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
          >


            <Link
              to='/services'
              className='nav-links'
              onClick={closeMobileMenu}
              style={navStyle}
            >
              SERVICES <i className='fas fa-caret-down' />
            </Link>
            {dropdown && <Dropdown />}
          </li>

                <li className='nav-item'>
                    <Link to='/kinesitherapeute' 
                    className='nav-links' 
                    style={navStyle}
                    onClick={closeMobileMenu}>
                        KINESITHERAPEUTES
                    </Link>
                </li>

                <li className='nav-item'>
                    <Link to='/Tarifs' 
                    className='nav-links'
                    style={navStyle} 
                    onClick={closeMobileMenu}>
                        TARIFS
                    </Link>
                </li>


                <li>
            <Link
              
              className='nav-links-mobile  button'
              onClick={closeMobileMenu}
              to=""
            >
              Connexion/ Inscription
            </Link>
          </li>
                

            </ul>
            <Link to=''> 
            <Button className="button" />
            </Link>

            <ul className='shop-icon'>
            <img className='shop' src={shopping}  />
            </ul>
            
            

        </nav>
        </>
    )
}
export default Navbar;