import Recat from 'react'
import mission from '../../../images/mission.jpg'

import './Mission.css'


function Mission() {
    return (
        <div>
            <div className="image">
                <img className="image-mision" src={mission} />
            </div>
            <div className="container">
                <div className="titre">
                    <h3 className="text-mission">NOTRE MISSION</h3>
                    <p className="paragraph">Nous connaissons tous les effets bénéfiques des médicaments naturels. 
                        naturels, ils nous permettent de rester en bonne santé <br/>et de rétablir
                        notre corps et notre esprit  </p>
                </div>

                <div className="paragraphe-mission">
                    <div className="hour">
                        <h2 className=" opening-hour" > Horaires d'ouvertures...</h2>

                    </div>

                    <div className="days">
                        <div className="clock">
                            <i className="fas fa-history white"></i>
                        </div>
                            <div className="blok-time-day">
                                <h1 className="time-day"> Lun-Sam: 10:30am-9:30pm <br/>
                                Dim: 11:00am-7:30pm </h1>
                            </div>
                        
                    </div>
                    
                </div>

                

        </div>
        </div>
    )
}


export default Mission;