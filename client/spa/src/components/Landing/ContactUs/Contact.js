import React from 'react'
import map from '../../../images/map.PNG'
import './Contact.css'


function Contact () {
    return(
        <>
            <div className="block-location">
                <h3 className="location"> LOCALISER LE SPA THERME</h3>
                    <div className="image-map">
                        <img className="maps" src={map} />
                     </div>

                     <div className="class1">
                         <div className="phone-icon">
                         <i className="fas fa-phone-alt"></i>
                         </div>
                     
                         <h1 className="phone"> Phones: +216 74 000 000
                         or +216 22 000 000</h1>
                     </div>

                     <div className="class2">
                         <div className="email-icon"> 
                         <i class="far fa-envelope"></i>
                         </div>
                        
                         <h1 className="email"> Email : spatherme@gmail.com</h1>
                     </div>
            </div>


            <div className="block-contact">
                <h3 className="contact"> CONTACTEZ-NOUS</h3>

            <div className='form'>
                <input
            className='form-inpute'
            type='text'
            name='name'
            placeholder='Nom'
            
          />
          </div>

          <div className='form'>
             <input
                className='form-inpute'
                type='text'
                name='Adress'
                placeholder='Adresse Email'
                        
            />
            </div>

            <div className='form'>
                <textarea className="text-description"
                placeholder='Description'>
                    
                </textarea>
             </div>

                
            <div className="btn-submit">
                <input className="submit" type="submit" value="Envoyer" />

                </div>
            </div>

           

            



        </>

    )
}


export default Contact