import React from 'react'

import spa from '../../../images/img-service.jfif'
import face from '../../../images/face-cream.png'
import massage from '../../../images/massage.png'
import massagespa from '../../../images/massage-spa.png'
import relax from '../../../images/relax.png'
import comp from '../../../images/comp.png'

import './Services.css'



function Services ()  {
   
    return (
        <div className="text">
            <div className="text-desc"><h5 className="text1"> Nous vous aidons à vous sentir renouvelé et 
            paraître sous votre meilleur jourt </h5> </div>

            <h4 className="text2"> Sanctuaire de beauté et de bien-être,
            Avec des rituels de <br/>relaxation dans tous nos services</h4>


            <div className="images">
                
                <img  className="image-spa"src={spa}  />
            </div>

            <div className="text-pro">
                <h4 className="text3"> NOUS SOMMES DES STYLISTES PROFESSIONNELS,
                ESTHETICIENS, <br/>et THÉRAPEUTEURS DE MASSAGE.</h4>
            </div>



             <div className="items-container-service">

                <div className="item-service">
                    <div className="image-icon">
                         <img  className="img-icon" src={relax}  />
                    </div>
                <span className="captions-img">MASSAGE BODY</span>
                </div> 

                <div className="item-service">
                    <div className="image-icon">    
                         <img  className="img-icon"src={face}  />
                    </div>
                <span className="captions-img">SKIN SPE CARE</span>
                </div> 

                <div className="item-service">
                    <div className="image-icon">    
                         <img  className="img-icon"src={comp}  />
                    </div>
                <span className="captions-img">COMP REFRESHMENT</span>
                </div> 


                <div className="item-service">
                    <div className="image-icon">    
                         <img  className="img-icon"src={massage}  />
                    </div>
                <span className="captions-img">SPA & FACIAL</span>
                </div> 

                <div className="item-service">
                    <div className="image-icon">    
                         <img  className="img-icon"src={massagespa}  />
                    </div>
                <span className="captions-img">MASSAGE THERAPY</span>
                </div> 

            </div> 
            

            

            


             
        </div>
        

       


        
    )
}

export default Services ;
