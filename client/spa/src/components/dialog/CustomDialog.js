import React from "react";
import { Modal, ModalTitle } from "react-bootstrap";
import ModalHeader from "react-bootstrap/esm/ModalHeader";

const CustomDialog = ({ show, handleClose, body, header, className }) => {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      className={`${className} modal-dialog-centered`}
    >
      <ModalHeader style={{ padding: "0rem 0.5rem 1rem 1rem" }}>
        <ModalTitle>{header}</ModalTitle>
        <button
          className="close  btn btn-secondary m-0"
          data-dismiss="modal"
          aria-label="Close"
          onClick={handleClose}
          style={{
            border: "none",
            fontSize: "26px",
          }}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </ModalHeader>
      <Modal.Body>{body}</Modal.Body>
    </Modal>
 
  );
};
export default CustomDialog;
