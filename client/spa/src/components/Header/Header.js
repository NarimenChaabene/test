import React from 'react'
import {Link} from 'react-router-dom'
import './Header.css'
import logo from '../../images/logo.png'
import shop_register from '../../images/shopping.png'

function Header() {

    return(
        <div>
            <div className="navbar-register">

                <div className="header-register">
                    <img src={logo} className='logo-register'/>
                </div>

                <div className="block-header">

                    <div className="text-register">
                        <p className="compte">VOUS AVEZ DEJA UN COMPTE</p>
                        
                    </div>
                    <div className="btn-register-header">
                        <Link to='/login'>
                        <button className="register-btn">Connexion</button>
                        </Link>

                    </div>
                </div>

                <div className="shop-register">
                    <img src={shop_register} className="logo-shop-register"/>
                 </div>

            </div>

        </div>
    )
}


export default Header