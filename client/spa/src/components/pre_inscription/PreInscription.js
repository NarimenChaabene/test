import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "react-validation/build/form";
import input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { preinscription} from  "../../redux/actions/auth_pre";
import {Link} from 'react-router-dom'
import Header from '../Header/Header'
import './PreInscription.css'
import Footer from  '../Landing/Footer/Footer'

const required = (value) => {
    if (!value) {
      return (
        <div className="alert alert-danger" role="alert">
          This field is required!
        </div>
      );
    }
  };

  const validEmail = (value) => {
    if (!isEmail(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a valid email.
        </div>
      );
    }
  };
  
  const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
      return (
        <div className="alert alert-danger" role="alert">
          The username must be between 3 and 20 characters.
        </div>
      );
    }
  };


  function PreInscription() {
    
  const form = useRef();
  const checkBtn = useRef();

  const [nom, setUsernom] = useState("");
  const [prenom, setUserprenom] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");
  const [Message, setMessage] = useState("");
  const [successful, setSuccessful] = useState(false);

  const { message } = useSelector(state => state.message);
  const dispatch = useDispatch();

  const onChangeProspectNom = (e) => {
    const nom = e.target.value;
    setUsernom(nom);
  };

  const onChangeProspectPrenom = (e) => {
    const prenom = e.target.value;
    setUserprenom(prenom,);
  };

  const onChangeProsppectEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const onChangeProspectTelephone = (e) => {
    const telephone = e.target.value;
    setTelephone(telephone);
  };

  const onChangeProspectMessage = (e) => {
    const Message = e.target.value;
    setMessage(Message);
  };

  const handlePreInscription = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(preinscription(nom,prenom, email, telephone,Message))
        .then(() => {
          setSuccessful(true);
        })
        .catch(() => {
          setSuccessful(false);
        });
    }
  };

    return(

      <div>
        <Header/>
        <Form onSubmit={handlePreInscription} ref={form}>
          {! successful && (
            <div className="body-preinscription">
              <div className="text-preinscription">
                  <div className="titre-preinscription">
                    <h1 className="h1-preinscription">
                      COMMENCEZ VOTRE EXPÉRIENCE AUJOURD'HUI
                    </h1>
                  </div>
                  <h3 className="text-service-preinscription">
                  Renseignez-vous pour plus d'informations sur l'offre spéciale ouverture pour les 
                  200 premiers inscrits et les services de votre <br/> nouveau
                   club de remise en forme premium.
                  </h3>

              </div>


              <div className="form-preinscription">

                <div className="blok1-preinscription">

                  <div className="nom-preinscription">
                    <label className="label-preinscription">NOM</label>
                    <br/>
                    <input 
                          type="text"
                          className="texte-preinscription"
                          placeholder=""
                          label="NOM"
                          name="nom"
                          value={nom}
                          onChange={onChangeProspectNom}
                          validations={[required, vusername]}
                          />
                  </div>



                  <div className="prenom-preinscription">

                    <label className="label-preinscription"> PRENOM</label> 

                    <br/>
                    <input 
                          type="text"
                          className="texte-preinscription"
                          placeholder=""
                          label="PRENOM"
                          name="prenom"
                          onChange={onChangeProspectPrenom}
                          validations={[required,vusername]}
                          />

                  </div>

                </div>


                <br/> <br/>

                <div className="blok2-preinscription">
                  <div className="email-presinscription">
                    <label className="label-preinscription">ADRESSE EMAIL</label>
                    <br/>

                    <input 
                          type="text"
                          className="texte-preinscription"
                          placeholder=""
                          label="ADRESSE EMAIL"
                          name="email"
                          value={email}
                          onChange={onChangeProsppectEmail}
                          validations={[required, validEmail]}
                          />

                    
                  </div>


                  <div className="telephone-preinscription">
                    <label className="label-preinscription"> Telephone </label>

                    <br/>
                    <input
                          type="text"
                          className="texte-preinscription"
                          placeholder=""
                          name="telephone"
                          value={telephone}
                          onChange={onChangeProspectTelephone}
                          validateAll={[required]}
                        />
                  </div>
                </div>

                <br/> <br/>


                <div className="blok3-preinscription">
                  <div className="message-preinscription">
                  <label className="label-preinscription"> Message </label>

                        <br/>
                        <textarea
                              type="text"
                              className="texte-preinscription"
                              placeholder=""
                              name="Message"
                              value={Message}
                              onChange={onChangeProspectMessage}
                              validateAll={[required]}
                            />

                  </div>
                </div>



                  <div className="boutton-preinscri">
                        <button className="btn-preinscription">Envoyer</button>
                  </div>


              </div>

              <Footer/>


            </div>
          )}
           {message && (
            <div className="form-group">
              <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }
        } ref={checkBtn} />


        </Form>

      </div>
    )
  }

  export default PreInscription