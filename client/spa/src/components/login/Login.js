import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { login } from "../../redux/actions/auth";

import Header from "../Header/Header";
import "./Login.css";
import Footer from "../Landing/Footer/Footer";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Login = (props) => {
  const form = useRef();
  const checkBtn = useRef();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const { isLoggedIn } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);

  const dispatch = useDispatch();

  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  console.log({isLoggedIn});
  const handleLogin = (e) => {
    e.preventDefault();

    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(login(email, password))
        .then(() => {
          props.history.push("/dashboard");
          // window.location.reload();
        })
        .catch(() => {
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  };

  // if (isLoggedIn) {
  //   return <Redirect to="/dashboard" />;
  // }

  return (
    <div>
      <Header />
      <div className="col-md-12">
        <div className="body-login">
          <div className="text-inscription">
            <div className="titre-inscri">
              <h1 className="h1-inscription">SE CONNECTER</h1>
            </div>

          
          </div>

          <Form onSubmit={handleLogin} ref={form}>
            <div className="blok2-login">
              <div className="email-login">
                <div className="aaa">
                  <label className="label-login">ADRESSE EMAIL</label>

                  <br />
                  <input
                    type="email"
                    className="texte-login"
                    placeholder=""
                    label="ADRESSE EMAIL"
                    name="email"
                    value={email}
                    onChange={onChangeEmail}
                    validations={[required]}
                  />
                </div>
              </div>

              <div className="password-login">
                <label className="label-login">MOT DE PASSE</label>

                <br />
                <input
                  type="password"
                  className="texte-login"
                  placeholder=""
                  label="MOT DE PASSE"
                  name="password"
                  value={password}
                  onChange={onChangePassword}
                  validations={[required]}
                />
              </div>
            </div>

            <div className="form-group">
              <button className="btn-login btn-block" disabled={loading}>
                {loading && <span className=""></span>}
                <span>connexion</span>
              </button>
            </div>

            {message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {message}
                </div>
              </div>
            )}
            <CheckButton style={{ display: "none" }} ref={checkBtn} />
          </Form>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default Login;
