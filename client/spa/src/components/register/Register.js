import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { register } from  "../../redux/actions/auth";
import {Link} from 'react-router-dom'
import Header from '../Header/Header'
import './Register.css'
import Footer from  '../Landing/Footer/Footer'

const required = (value) => {
    if (!value) {
      return (
        <div className="alert alert-danger" role="alert">
          This field is required!
        </div>
      );
    }
  };
  
  const validEmail = (value) => {
    if (!isEmail(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a valid email.
        </div>
      );
    }
  };
  
  const vusername = (value) => {
    if (value.length < 3 || value.length > 20) {
      return (
        <div className="alert alert-danger" role="alert">
          The username must be between 3 and 20 characters.
        </div>
      );
    }
  };
  
  const vpassword = (value) => {
    if (value.length < 6 || value.length > 40) {
      return (
        <div className="alert alert-danger" role="alert">
          The password must be between 6 and 40 characters.
        </div>
      );
    }
  };


  const vrole = (value) => {
    if (value !=='admin' || value !=='client') {
      return (
        <div className="alert alert-danger" role="alert">
          Check your Role.
        </div>
      );
    }
  };

function Register () {

  const form = useRef();
  const checkBtn = useRef();

  const [nom, setUsernom] = useState("");
  const [prenom, setUserprenom] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [successful, setSuccessful] = useState(false);

  const { message } = useSelector(state => state.message);
  const dispatch = useDispatch();

  const onChangeUsernom = (e) => {
    const nom = e.target.value;
    setUsernom(nom);
  };

  const onChangeUserprenom = (e) => {
    const prenom = e.target.value;
    setUserprenom(prenom,);
  };

  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const onChangeRole = (e) => {
    const role = e.target.value;
    setRole(role);
  };

  const handleRegister = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(register(nom,prenom, email, password,role))
        .then(() => {
          setSuccessful(true);
        })
        .catch(() => {
          setSuccessful(false);
        });
    }
  };

    return(
        <div>
              <Header/>
            <Form onSubmit={handleRegister} ref={form}> 
                {!successful && (
            
                <div className="body-register">
                    <div className="text-inscription">
                        <div className="titre-inscri">
                            <h1 className="h1-inscription">
                                INSCRIPTION
                            </h1>
                        </div>

                        <h3 className="texte-service">Afin d’accéder aux services 
                            SPA THERME, merci de bien vouloir vous inscrire
                        </h3>


                    </div>




                    <div className="form-register">

                        <div className="blok1-register">
                            <div className="nom-register">
                        
                                <label className="label-register">NOM</label>
                
                                <br/>
                                <input
                                    type="text"
                                    className="texte-register"
                                    placeholder=""
                                    label="NOM"
                                    name="nom"
                                    value={nom}
                                    onChange={onChangeUsernom}
                                    validations={[required, vusername]}
                                />
                            </div>
                            

                            
                            <div className="prenom-register">
                                <label className="label-register">PRENOM</label>
                
                                <br/>
                                <input
                                    type="text"
                                    className="texte-register"
                                    placeholder=""
                                    label="PRENOM"
                                    name="prenom"
                                    value={prenom}
                                    onChange={onChangeUserprenom}
                                    validations={[required, vusername]}
                                />
                            </div>



                            
                        </div>

                        <br/><br/>
                        <div className="blok2-register">
                            <div className="email-register">
                                <div className="aaa">
                        
                                    <label className="label-register">ADRESSE EMAIL</label>
                    
                                    <br/>
                                    <input
                                        type="email"
                                        className="texte-register"
                                        placeholder=""
                                        label="ADRESSE EMAIL"
                                        name="email"
                                        value={email}
                                        onChange={onChangeEmail}
                                        validations={[required, validEmail]}



                                    />
                                </div>
                            </div>
                            

                            
                            <div className="password-register">
                                <label className="label-register">MOT DE PASSE</label>
                
                                <br/>
                                <input
                                    type="password"
                                    className="texte-register"
                                    placeholder=""
                                    label="MOT DE PASSE"
                                    name="password"
                                    value={password}
                                    onChange={onChangePassword}
                                    validations={[required, vpassword]}
                                />
                            </div>


                            <div className="role-register">
                                <label className="label-register">ROLE</label>
                
                                <br/>
                                <input
                                    type="text"
                                    className="texte-register"
                                    placeholder=""
                                    label="ROLE"
                                    name="role"
                                    value={role}
                                    onChange={onChangeRole}
                                    validations={[required, vrole]}
                                />
                            </div>



                            
                        </div>

                        <div className="case-cocher">
                            <input type="checkbox" id="check1" name="" value=""/>
                            <label for=""> J'accepte de recevoir toute 
                            l'actualiaté de la part de SPA THERME (planing , évènemnets..)</label>
                        </div>


                        <div className="boutton-inscri">
                            <button className="btn-inscription">JE M'INSCRIS</button>
                           
                        </div>

                    </div>
                    <Footer/>
                    
                </div>
                )}
                {message && (
            <div className="form-group">
              <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }
        } ref={checkBtn} />

           
           </Form>
        </div>
       
        
        

    )
}

export default Register