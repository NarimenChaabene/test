import axios from "axios";
import { API_URI} from "../../config";
import {
  LIST_PROSPECT_FAIL,
  LIST_PROSPECT_SUCCESS,
  LIST_PROSPECT_LOADING,
  DELETE_PROSPECT_SUCCESS,
  DELETE_PROSPECT_FAIL,
  ADD_PROSPECT_SUCCESS,
  ADD_PROSPECT_FAIL,
  DETAILS_PROSPECT_SUCCESS,
  DETAILS_PROSPECT_FAIL,
  UPDATE_PROSPECT_SUCCESS,
  UPDATE_PROSPECT_FAIL,
} from "./types";
/*const initialState = {
    allProspect: [],
     detailsProspect: null,
     success: null,
    loading: null,
    failure: null,
}*/

export const getAllProspect = (token) => {
    const options = {
      headers: { "auth-token": token },
    };
    return (dispatch) => {
      dispatch({
        type: LIST_PROSPECT_LOADING,
      });
      return axios
        .get(`${API_URI}prospect`, options)
        .then((response) => {
          dispatch({
            type: LIST_PROSPECT_SUCCESS,
            payload: response.data,
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: LIST_PROSPECT_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };

  export const deleteProspect = (id, token) => {
    const options = {
      headers: { "auth-token": token },
    };
    return (dispatch) => {
      return axios
        .delete(`${API_URI}prospect/${id}`, options)
        .then((response) => {
          dispatch({
            type: DELETE_PROSPECT_SUCCESS,
            payload: { message: response.data, id },
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: DELETE_PROSPECT_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };



  export const addProspect= (nom, prenom, email,telephone,Message, token) => {
    const options = {
      headers: { "auth-token": token },
    };
    return (dispatch) => {
      return axios
        .post(`${API_URI}prospect`, { options, nom, prenom, email,telephone,Message, })
        .then((response) => {
          dispatch({
            type: ADD_PROSPECT_SUCCESS,
            payload: response.data,
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: ADD_PROSPECT_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };


  export const getProspect = (id) => {
    return (dispatch) => {
      return axios
        .get(`${API_URI}prospect/${id}`)
        .then((response) => {
          dispatch({
            type: DETAILS_PROSPECT_SUCCESS,
            payload: response.data,
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: DETAILS_PROSPECT_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };


  export const updateProspect = (id, data) => {
    return (dispatch) => {
      return axios
        .put(`${API_URI}prospect/${id}`, data)
        .then((response) => {
          dispatch({
            type: UPDATE_PROSPECT_SUCCESS,
            payload: response.data,
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: UPDATE_PROSPECT_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };