import {
  DELETE_CATEGORIE_FAIL,
  DELETE_CATEGORIE_SUCCESS,
  LIST_CATEGORIE_FAIL,
  LIST_CATEGORIE_LOADING,
  LIST_CATEGORIE_SUCCESS,
  ADD_CATEGORIE_SUCCESS,
  ADD_CATEGORIE_FAIL,
  DETAILS_CATEGORIE_SUCCESS,
  DETAILS_CATEGORIE_FAIL,
  UPDATE_CATEGORIE_SUCCESS,
  UPDATE_CATEGORIE_FAIL,
} from "../actions/types";

const initialState = {
  allCategories: [],
  detailsCategorie: null,
  success: null,
  loading: null,
  failure: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LIST_CATEGORIE_LOADING:
      return {
        ...state,
        loading: true,
        success: false,
        failure: false,
      };
    case LIST_CATEGORIE_SUCCESS:
      return {
        ...state,
        allCategories: payload,
        loading: false,
        success: true,
        failure: false,
      };
    case LIST_CATEGORIE_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case DELETE_CATEGORIE_SUCCESS:
      const filteredData = state.allCategories.filter(
        (c) => c._id !== payload.id
      );
      return {
        ...state,
        allCategories: filteredData,
        success: true,
        failure: false,
        loading: false,
      };
    case DELETE_CATEGORIE_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case ADD_CATEGORIE_SUCCESS:
      const newData = state.allCategories.push(payload);
      return {
        ...state,
        allCategories: newData,
        success: true,
        failure: false,
        loading: false,
      };
    case ADD_CATEGORIE_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case DETAILS_CATEGORIE_SUCCESS:
      return {
        ...state,
        detailsCategorie: payload,
        success: true,
        failure: false,
        loading: false,
      };
    case DETAILS_CATEGORIE_FAIL:
      return {
        ...state,
        detailsCategorie: null,
        success: false,
        failure: true,
        loading: false,
      };
    case UPDATE_CATEGORIE_SUCCESS:
      return {
        ...state,
        detailsCategorie: payload,
        success: true,
        failure: false,
        loading: false,
      };
    case UPDATE_CATEGORIE_FAIL:
      return {
        ...state,
        detailsCategorie: null,
        success: false,
        failure: true,
        loading: false,
      };
    default:
      return state;
  }
}
