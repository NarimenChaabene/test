import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import categorie from "./categorie";
import membre from "./membre";
import kine from "./kine";
import seance from "./seance";
import feedback from './FeedbackCient'
import updateProfileClient from './EditProfile'




export default combineReducers({
  auth,
  message,
  categorie,
  membre,
  kine,
  seance,
  feedback,
  updateProfileClient


  
});