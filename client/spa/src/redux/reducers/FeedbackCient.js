import {
   
  LIST_FEEDBACK_FAIL,
  LIST_FEEDBACK_LOADING,
  LIST_FEEDBACK_SUCCESS,
    ADD_FEEDBACK_SUCCESS,
    ADD_FEEDBACK_FAIL
  
  } from "../actions/types";
  
  const initialState = {
    allFeedbacks: [],
    detailsFeedbacks: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case LIST_FEEDBACK_LOADING:
        return {
          ...state,
          loading: true,
          success: false,
          failure: false,
        };
      case LIST_FEEDBACK_SUCCESS:
        return {
          ...state,
          allFeedbacks: payload,
          loading: false,
          success: true,
          failure: false,
        };
      case LIST_FEEDBACK_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      
      
      
      case ADD_FEEDBACK_SUCCESS:
        const newData = state.allFeedbacks.push(payload);
        return {
          ...state,
          allFeedbacks: newData,
          success: true,
          failure: false,
          loading: false,
        };
      case ADD_FEEDBACK_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      
      default:
        return state;
    }
  }
  