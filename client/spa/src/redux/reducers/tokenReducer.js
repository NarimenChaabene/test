
const initToken = {
    token: "",
}
export function tokenReducer(state=initToken, action) {
    switch(action.type) {
        case "SAVE_TOKEN": return {
            token: action.info
        }
        default: return state
    }
}