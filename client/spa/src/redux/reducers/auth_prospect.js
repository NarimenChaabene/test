import {
    RPREINSCRIPTION_PROSPECT_SUCCESS,
    PREINSCRIPTION_PROSPECT_FAILL,
  } from "../actions/types";
  
  const prospect = JSON.parse(localStorage.getItem("prospect"));
 
  
  const initialState = prospect
    ? { isLoggedIn: true, prospect }
    
    : { isLoggedIn: false, prospect: null };


    
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case RPREINSCRIPTION_PROSPECT_SUCCESS:
        return {
          ...state,
          isLoggedIn: false,
        };
      case PREINSCRIPTION_PROSPECT_FAIL:
        return {
          ...state,
          isLoggedIn: false,
        };
      
  }
}