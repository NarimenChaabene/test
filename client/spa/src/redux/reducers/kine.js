import {
    DELETE_KINE_FAIL,
    DELETE_KINE_SUCCESS,
    LIST_KINE_FAIL,
    LIST_KINE_LOADING,
    LIST_KINE_SUCCESS,
    ADD_KINE_SUCCESS,
    ADD_KINE_FAIL,
    DETAILS_KINE_SUCCESS,
    DETAILS_KINE_FAIL,
    UPDATE_KINE_SUCCESS,
    UPDATE_KINE_FAIL,
  } from "../actions/types";
  
  const initialState = {
    allKines: [],
    detailsKine: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case LIST_KINE_LOADING:
        return {
          ...state,
          loading: true,
          success: false,
          failure: false,
        };
      case LIST_KINE_SUCCESS:
        return {
          ...state,
          allKines: payload,
          loading: false,
          success: true,
          failure: false,
        };
      case LIST_KINE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DELETE_KINE_SUCCESS:
        const filteredData = state.allKines.filter(
          (c) => c._id !== payload.id
        );
        return {
          ...state,
          allKines: filteredData,
          success: true,
          failure: false,
          loading: false,
        };
      case DELETE_KINE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case ADD_KINE_SUCCESS:
        const newData = state.allKines.push(payload);
        return {
          ...state,
          allKines: newData,
          success: true,
          failure: false,
          loading: false,
        };
      case ADD_KINE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DETAILS_KINE_SUCCESS:
        return {
          ...state,
          detailsKine: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case DETAILS_KINE_FAIL:
        return {
          ...state,
          detailsKine: null,
          success: false,
          failure: true,
          loading: false,
        };
      case UPDATE_KINE_SUCCESS:
        return {
          ...state,
          detailsKine: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case UPDATE_KINE_FAIL:
        return {
          ...state,
          detailsKine: null,
          success: false,
          failure: true,
          loading: false,
        };
      default:
        return state;
    }
  }
  