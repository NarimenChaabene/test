import {
  
    LIST_FEEDBACK_FAIL,
    LIST_FEEDBACK_LOADING,
    LIST_FEEDBACK_SUCCESS,
  
    DETAILS_FEEDBACK_SUCCESS,
    DETAILS_FEEDBACK_FAIL,

    DELETE_FEEDBACK_FAIL,
  DELETE_FEEDBACK_SUCCESS,

  } from "../actions/types";
  
  const initialState = {
    allFeedback: [],
    detailsFeedback: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case LIST_FEEDBACK_LOADING:
        return {
          ...state,
          loading: true,
          success: false,
          failure: false,
        };
      case LIST_FEEDBACK_SUCCESS:
        return {
          ...state,
          allFeedback: payload,
          loading: false,
          success: true,
          failure: false,
        };
      case LIST_FEEDBACK_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };

        case DETAILS_FEEDBACK_SUCCESS:
            return {
              ...state,
              detailsFeedback: payload,
              success: true,
              failure: false,
              loading: false,
            };
      
      
      
      case DETAILS_FEEDBACK_FAIL:
        return {
          ...state,
          detailsCategorie: null,
          success: false,
          failure: true,
          loading: false,
        };
      
        case DELETE_FEEDBACK_SUCCESS:
          const filteredData = state.allFeedback.filter(
            (c) => c._id !== payload.id
          );
          return {
            ...state,
            allFeedback: filteredData,
            success: true,
            failure: false,
            loading: false,
          };
        case DELETE_FEEDBACK_FAIL:
          return {
            ...state,
            success: false,
            failure: true,
            loading: false,
          };
     
      default:
        return state;
    }
  }
  