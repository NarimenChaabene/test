import {
  DELETE_RESERVATION_FAIL,
  DELETE_RESERVATION_SUCCESS,
  LIST_RESERVATION_FAIL,
  LIST_RESERVATION_LOADING,
  LIST_RESERVATION_SUCCESS,
  ADD_RESERVATION_SUCCESS,
  ADD_RESERVATION_FAIL,
  DETAILS_RESERVATION_SUCCESS,
  DETAILS_RESERVATION_FAIL,
  UPDATE_RESERVATION_SUCCESS,
  UPDATE_RESERVATION_FAIL,
} from "../actions/types";

const initialState = {
  allReservations: [],
  detailsReservation: null,
  success: null,
  loading: null,
  failure: null,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LIST_RESERVATION_LOADING:
      return {
        ...state,
        loading: true,
        success: false,
        failure: false,
      };
    case LIST_RESERVATION_SUCCESS:
      return {
        ...state,
        allReservations: payload,
        loading: false,
        success: true,
        failure: false,
      };
    case LIST_RESERVATION_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case DELETE_RESERVATION_SUCCESS:
      const filteredData = state.allReservations.filter(
        (c) => c._id !== payload.id
      );
      return {
        ...state,
        allReservations: filteredData,
        success: true,
        failure: false,
        loading: false,
      };
    case DELETE_RESERVATION_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case ADD_RESERVATION_SUCCESS:
      const newData = state.allReservations.push(payload);
      return {
        ...state,
        allReservations: newData,
        success: true,
        failure: false,
        loading: false,
      };
    case ADD_RESERVATION_FAIL:
      return {
        ...state,
        success: false,
        failure: true,
        loading: false,
      };
    case DETAILS_RESERVATION_SUCCESS:
      return {
        ...state,
        detailsReservation: payload,
        success: true,
        failure: false,
        loading: false,
      };
    case DETAILS_RESERVATION_FAIL:
      return {
        ...state,
        detailsReservation: null,
        success: false,
        failure: true,
        loading: false,
      };
    case UPDATE_RESERVATION_SUCCESS:
      return {
        ...state,
        detailsReservation: payload,
        success: true,
        failure: false,
        loading: false,
      };
    case UPDATE_RESERVATION_FAIL:
      return {
        ...state,
        detailsReservation: null,
        success: false,
        failure: true,
        loading: false,
      };
    default:
      return state;
  }
}
