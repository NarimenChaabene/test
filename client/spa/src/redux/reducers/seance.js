import {
    DELETE_SEANCE_FAIL,
    DELETE_SEANCE_SUCCESS,
    LIST_SEANCE_FAIL,
    LIST_SEANCE_LOADING,
    LIST_SEANCE_SUCCESS,
    ADD_SEANCE_SUCCESS,
    ADD_SEANCE_FAIL,
    DETAILS_SEANCE_SUCCESS,
    DETAILS_SEANCE_FAIL,
    UPDATE_SEANCE_SUCCESS,
    UPDATE_SEANCE_FAIL,
  } from "../actions/types";
  
  const initialState = {
    allSeances: [],
    detailsSeance: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case LIST_SEANCE_LOADING:
        return {
          ...state,
          loading: true,
          success: false,
          failure: false,
        };
      case LIST_SEANCE_SUCCESS:
        return {
          ...state,
          allSeances: payload,
          loading: false,
          success: true,
          failure: false,
        };
      case LIST_SEANCE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DELETE_SEANCE_SUCCESS:
        const filteredData = state.allSeances.filter(
          (c) => c._id !== payload.id
        );
        return {
          ...state,
          allSeances: filteredData,
          success: true,
          failure: false,
          loading: false,
        };
      case DELETE_SEANCE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case ADD_SEANCE_SUCCESS:
        const newData = state.allSeances.push(payload);
        return {
          ...state,
          allSeances: newData,
          success: true,
          failure: false,
          loading: false,
        };
      case ADD_SEANCE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DETAILS_SEANCE_SUCCESS:
        return {
          ...state,
          detailsSeance: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case DETAILS_SEANCE_FAIL:
        return {
          ...state,
          detailsSeance: null,
          success: false,
          failure: true,
          loading: false,
        };
      case UPDATE_SEANCE_SUCCESS:
        return {
          ...state,
          detailsSeance: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case UPDATE_SEANCE_FAIL:
        return {
          ...state,
          detailsSeance: null,
          success: false,
          failure: true,
          loading: false,
        };
      default:
        return state;
    }
  }
  