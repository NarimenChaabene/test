import {
    DELETE_MEMBRE_FAIL,
    DELETE_MEMBRE_SUCCESS,
    LIST_MEMBRE_FAIL,
    LIST_MEMBRE_LOADING,
    LIST_MEMBRE_SUCCESS,
    ADD_MEMBRE_SUCCESS,
    ADD_MEMBRE_FAIL,
    DETAILS_MEMBRE_SUCCESS,
    DETAILS_MEMBRE_FAIL,
    UPDATE_MEMBRE_SUCCESS,
    UPDATE_MEMBRE_FAIL,
  } from "../actions/types";
  
  const initialState = {
    allMembres: [],
    detailsMembre: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case LIST_MEMBRE_LOADING:
        return {
          ...state,
          loading: true,
          success: false,
          failure: false,
        };
      case LIST_MEMBRE_SUCCESS:
        return {
          ...state,
          allMembres: payload,
          loading: false,
          success: true,
          failure: false,
        };
      case LIST_MEMBRE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DELETE_MEMBRE_SUCCESS:
        const filteredData = state.allMembres.filter(
          (c) => c._id !== payload.id
        );
        return {
          ...state,
          allMembres: filteredData,
          success: true,
          failure: false,
          loading: false,
        };
      case DELETE_MEMBRE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case ADD_MEMBRE_SUCCESS:
        const newData = state.allMembres.push(payload);
        return {
          ...state,
          allMembres: newData,
          success: true,
          failure: false,
          loading: false,
        };
      case ADD_MEMBRE_FAIL:
        return {
          ...state,
          success: false,
          failure: true,
          loading: false,
        };
      case DETAILS_MEMBRE_SUCCESS:
        return {
          ...state,
          detailsMembre: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case DETAILS_MEMBRE_FAIL:
        return {
          ...state,
          detailsMembre: null,
          success: false,
          failure: true,
          loading: false,
        };
      case UPDATE_MEMBRE_SUCCESS:
        return {
          ...state,
          detailsMembre: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case UPDATE_MEMBRE_FAIL:
        return {
          ...state,
          detailsMembre: null,
          success: false,
          failure: true,
          loading: false,
        };
      default:
        return state;
    }
  }
  