import {
    
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAIL,
  } from "../actions/types";
  
  const initialState = {
    allProfile: [],
    detailsProfile: null,
    success: null,
    loading: null,
    failure: null,
  };
  
  export default function (state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case UPDATE_PROFILE_SUCCESS:
        return {
          ...state,
          detailsProfile: payload,
          success: true,
          failure: false,
          loading: false,
        };
      case UPDATE_PROFILE_FAIL:
        return {
          ...state,
          detailsProfile: null,
          success: false,
          failure: true,
          loading: false,
        };
      default:
        return state;
    }
  }
  