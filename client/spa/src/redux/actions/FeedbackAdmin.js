import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_FEEDBACK_FAIL,
  LIST_FEEDBACK_SUCCESS,
  LIST_FEEDBACK_LOADING,
  
  DETAILS_FEEDBACK_SUCCESS,
  DETAILS_FEEDBACK_FAIL,

  DELETE_FEEDBACK_SUCCESS,
  DELETE_FEEDBACK_FAIL,

} from "./types";

export const getAllFeedback = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_FEEDBACK_LOADING,
    });
    return axios
      .get(`${API_URI}feedback`, options)
      .then((response) => {
        dispatch({
          type: LIST_FEEDBACK_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_FEEDBACK_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
export const getFeedback = (id) => {
    return (dispatch) => {
      return axios
        .get(`${API_URI}feedback/${id}`)
        .then((response) => {
          dispatch({
            type: DETAILS_FEEDBACK_SUCCESS,
            payload: response.data,
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: DETAILS_FEEDBACK_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };

  export const deleteFeedback = (id, token) => {
    const options = {
      headers: { "auth-token": token },
    };
    return (dispatch) => {
      return axios
        .delete(`${API_URI}feedback/${id}`, options)
        .then((response) => {
          dispatch({
            type: DELETE_FEEDBACK_SUCCESS,
            payload: { message: response.data, id },
          });
          return true;
        })
        .catch((error) => {
          dispatch({
            type: DELETE_FEEDBACK_FAIL,
            payload: error.response,
          });
          return false;
        });
    };
  };