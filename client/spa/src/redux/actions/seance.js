import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_SEANCE_FAIL,
  LIST_SEANCE_SUCCESS,
  LIST_SEANCE_LOADING,
  DELETE_SEANCE_SUCCESS,
  DELETE_SEANCE_FAIL,
  ADD_SEANCE_SUCCESS,
  ADD_SEANCE_FAIL,
  DETAILS_SEANCE_SUCCESS,
  DETAILS_SEANCE_FAIL,
  UPDATE_SEANCE_SUCCESS,
  UPDATE_SEANCE_FAIL,
} from "./types";

export const getAllSeances = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_SEANCE_LOADING,
    });
    return axios
      .get(`${API_URI}seance`, options)
      .then((response) => {
        dispatch({
          type: LIST_SEANCE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_SEANCE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const deleteSeance = (id, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .delete(`${API_URI}seance/${id}`, options)
      .then((response) => {
        dispatch({
          type: DELETE_SEANCE_SUCCESS,
          payload: { message: response.data, id },
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DELETE_SEANCE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const addSeance = (data, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  const {
    Description,
    prix,
    dateDebut,
    dateFin,
    nbr_Place_Max,
    nbr_Reservation,
  } = data;
  return (dispatch) => {
    return axios
      .post(`${API_URI}seance`, {
        options,
        Description,
        prix,
        dateDebut,
        dateFin,
        nbr_Place_Max,
        nbr_Reservation,
      })
      .then((response) => {
        dispatch({
          type: ADD_SEANCE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_SEANCE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const getSeance = (id) => {
  return (dispatch) => {
    return axios
      .get(`${API_URI}seance/${id}`)
      .then((response) => {
        dispatch({
          type: DETAILS_SEANCE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DETAILS_SEANCE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const updateSeance = ({ id, data, token }) => {
  const options = {
    headers: { "auth-token": token },
  };
  const {
    Description,
    prix,
    nbr_Place_Max,
    nbr_Reservation,
    dateDebut,
    dateFin,
  } = data;
  return (dispatch) => {
    return axios
      .put(`${API_URI}seance/${id}`, {
        options,
        Description,
        prix,
        nbr_Place_Max,
        nbr_Reservation,
        dateDebut,
        dateFin,
      })
      .then((response) => {
        dispatch({
          type: UPDATE_SEANCE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_SEANCE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
