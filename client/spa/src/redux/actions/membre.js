import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_MEMBRE_FAIL,
  LIST_MEMBRE_SUCCESS,
  LIST_MEMBRE_LOADING,
  DELETE_MEMBRE_SUCCESS,
  DELETE_MEMBRE_FAIL,
  ADD_MEMBRE_SUCCESS,
  ADD_MEMBRE_FAIL,
  DETAILS_MEMBRE_SUCCESS,
  DETAILS_MEMBRE_FAIL,
  UPDATE_MEMBRE_SUCCESS,
  UPDATE_MEMBRE_FAIL,
} from "./types";

export const getAllMembres = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_MEMBRE_LOADING,
    });
    return axios
      .get(`${API_URI}client`, options)
      .then((response) => {
        dispatch({
          type: LIST_MEMBRE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_MEMBRE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const deleteMembre = (id, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .delete(`${API_URI}client/${id}`, options)
      .then((response) => {
        dispatch({
          type: DELETE_MEMBRE_SUCCESS,
          payload: { message: response.data, id },
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DELETE_MEMBRE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const addMembre = (data, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  const { nom, prenom, email, password } = data;
  return (dispatch) => {
    return axios
      .post(`${API_URI}client`, {
        options,
        nom,
        prenom,
        email,
        password,
      })
      .then((response) => {
        dispatch({
          type: ADD_MEMBRE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_MEMBRE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const getMembre = (id) => {
  return (dispatch) => {
    return axios
      .get(`${API_URI}client/${id}`)
      .then((response) => {
        dispatch({
          type: DETAILS_MEMBRE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DETAILS_MEMBRE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const updateMembre = ({ id, data, token }) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .put(`${API_URI}client/${id}`, {
        options,
        nom: data.nom,
        prenom: data.prenom,
        email: data.email,
      })
      .then((response) => {
        dispatch({
          type: UPDATE_MEMBRE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_MEMBRE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
