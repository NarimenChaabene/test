import axios from "axios";
import { API_URI } from "../../config";
import {
    LIST_FEEDBACK_FAIL,
    LIST_FEEDBACK_SUCCESS,
    LIST_FEEDBACK_LOADING,
    ADD_FEEDBACK_SUCCESS,
    ADD_FEEDBACK_FAIL
} from "./types";


export const getAllFeedback = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_FEEDBACK_LOADING,
    });
    return axios
      .get(`${API_URI}feedbackClient`, options)
      .then((response) => {
        dispatch({
          type: LIST_FEEDBACK_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_FEEDBACK_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};



export const addFeedback = (message, rating, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .post(`${API_URI}feedbackClient`, { options, message, rating })
      .then((response) => {
        dispatch({
          type: ADD_FEEDBACK_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_FEEDBACK_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

