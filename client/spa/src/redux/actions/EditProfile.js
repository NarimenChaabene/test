import axios from "axios";
import { API_URI } from "../../config";
import {
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAIL
} from "./types";


export const updateProfile = (id, data) => {
  return (dispatch) => {
    return axios
      .put(`${API_URI}editProfileClient/${id}`, data)
      .then((response) => {
        dispatch({
          type: UPDATE_PROFILE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_PROFILE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
