import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_CATEGORIE_FAIL,
  LIST_CATEGORIE_SUCCESS,
  LIST_CATEGORIE_LOADING,
  DELETE_CATEGORIE_SUCCESS,
  DELETE_CATEGORIE_FAIL,
  ADD_CATEGORIE_SUCCESS,
  ADD_CATEGORIE_FAIL,
  DETAILS_CATEGORIE_SUCCESS,
  DETAILS_CATEGORIE_FAIL,
  UPDATE_CATEGORIE_SUCCESS,
  UPDATE_CATEGORIE_FAIL,
} from "./types";

export const getAllCategories = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_CATEGORIE_LOADING,
    });
    return axios
      .get(`${API_URI}categorie`, options)
      .then((response) => {
        dispatch({
          type: LIST_CATEGORIE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_CATEGORIE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const deleteCategorie = (id, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .delete(`${API_URI}categorie/${id}`, options)
      .then((response) => {
        dispatch({
          type: DELETE_CATEGORIE_SUCCESS,
          payload: { message: response.data, id },
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DELETE_CATEGORIE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const addCategorie = (Description, Titre, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .post(`${API_URI}categorie`, { options, Description, Titre })
      .then((response) => {
        dispatch({
          type: ADD_CATEGORIE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_CATEGORIE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const getCategorie = (id) => {
  return (dispatch) => {
    return axios
      .get(`${API_URI}categorie/${id}`)
      .then((response) => {
        dispatch({
          type: DETAILS_CATEGORIE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DETAILS_CATEGORIE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const updateCategorie = (id, data) => {
  return (dispatch) => {
    return axios
      .put(`${API_URI}categorie/${id}`, data)
      .then((response) => {
        dispatch({
          type: UPDATE_CATEGORIE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_CATEGORIE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
