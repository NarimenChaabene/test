import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_RESERVATION_FAIL,
  LIST_RESERVATION_SUCCESS,
  LIST_RESERVATION_LOADING,
  DELETE_RESERVATION_SUCCESS,
  DELETE_RESERVATION_FAIL,
  ADD_RESERVATION_SUCCESS,
  ADD_RESERVATION_FAIL,
  DETAILS_RESERVATION_SUCCESS,
  DETAILS_RESERVATION_FAIL,
  UPDATE_RESERVATION_SUCCESS,
  UPDATE_RESERVATION_FAIL,
} from "./types";

export const getAllReservations = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_RESERVATION_LOADING,
    });
    return axios
      .get(`${API_URI}reservation`, options)
      .then((response) => {
        dispatch({
          type: LIST_RESERVATION_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_RESERVATION_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const deleteReservation = (id, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .delete(`${API_URI}reservation/${id}`, options)
      .then((response) => {
        dispatch({
          type: DELETE_RESERVATION_SUCCESS,
          payload: { message: response.data, id },
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DELETE_RESERVATION_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const addReservation = (data, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  const {
    Description,
    etat,
    dateReservation,
    Heure,
  } = data;
  return (dispatch) => {
    return axios
      .post(`${API_URI}reservation`, {
        options,
        Description,
        etat,
        dateReservation,
        Heure,
      })
      .then((response) => {
        dispatch({
          type: ADD_RESERVATION_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_RESERVATION_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const getReservation = (id) => {
  return (dispatch) => {
    return axios
      .get(`${API_URI}reservation/${id}`)
      .then((response) => {
        dispatch({
          type: DETAILS_RESERVATION_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DETAILS_RESERVATION_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const updateReservation = ({ id, data, token }) => {
  const options = {
    headers: { "auth-token": token },
  };
  const {
    Description,
    prix,
    nbr_Place_Max,
    nbr_Reservation,
    dateDebut,
    dateFin,
  } = data;
  return (dispatch) => {
    return axios
      .put(`${API_URI}reservation/${id}`, {
        options,
        Description,
        prix,
        nbr_Place_Max,
        nbr_Reservation,
        dateDebut,
        dateFin,
      })
      .then((response) => {
        dispatch({
          type: UPDATE_RESERVATION_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_RESERVATION_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
