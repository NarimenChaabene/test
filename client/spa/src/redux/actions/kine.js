import axios from "axios";
import { API_URI } from "../../config";
import {
  LIST_KINE_FAIL,
  LIST_KINE_SUCCESS,
  LIST_KINE_LOADING,
  DELETE_KINE_SUCCESS,
  DELETE_KINE_FAIL,
  ADD_KINE_SUCCESS,
  ADD_KINE_FAIL,
  DETAILS_KINE_SUCCESS,
  DETAILS_KINE_FAIL,
  UPDATE_KINE_SUCCESS,
  UPDATE_KINE_FAIL,
} from "./types";

export const getAllKines = (token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    dispatch({
      type: LIST_KINE_LOADING,
    });
    return axios
      .get(`${API_URI}kinesitherapeute`, options)
      .then((response) => {
        dispatch({
          type: LIST_KINE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: LIST_KINE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const deleteKine = (id, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .delete(`${API_URI}kinesitherapeute/${id}`, options)
      .then((response) => {
        dispatch({
          type: DELETE_KINE_SUCCESS,
          payload: { message: response.data, id },
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DELETE_KINE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const addKine = (data, token) => {
  const options = {
    headers: { "auth-token": token },
  };
  const { nom, prenom, email, password, numContact, specialite } = data;
  return (dispatch) => {
    return axios
      .post(`${API_URI}kinesitherapeute`, {
        options,
        nom,
        prenom,
        email,
        password,
        numContact,
        specialite,
      })
      .then((response) => {
        dispatch({
          type: ADD_KINE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: ADD_KINE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const getKine = (id) => {
  return (dispatch) => {
    return axios
      .get(`${API_URI}kinesitherapeute/${id}`)
      .then((response) => {
        dispatch({
          type: DETAILS_KINE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: DETAILS_KINE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};

export const updateKine = ({ id, data, token }) => {
  const options = {
    headers: { "auth-token": token },
  };
  return (dispatch) => {
    return axios
      .put(`${API_URI}kinesitherapeute/${id}`, {
        options,
        nom: data.nom,
        prenom: data.prenom,
        email: data.email,
        numContact: data.numContact,
        specialite: data.specialite,
      })
      .then((response) => {
        dispatch({
          type: UPDATE_KINE_SUCCESS,
          payload: response.data,
        });
        return true;
      })
      .catch((error) => {
        dispatch({
          type: UPDATE_KINE_FAIL,
          payload: error.response,
        });
        return false;
      });
  };
};
