import {
  PREINSCRIPTION_PROSPECT_SUCCESS,
  PREINSCRIPTION_PROSPECT_FAIL,
 
  SET_MESSAGE,
} from "./types";

import AuthProspect from "../../services/auth.prospect";

export const preinscription = (nom,prenom, email, telephone, Message) => (dispatch) => {
  return AuthProspect.preinscription(nom,prenom, email, telephone , Message).then(
    (response) => {
      dispatch({
        type: PREINSCRIPTION_PROSPECT_SUCCESS,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.data.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: PREINSCRIPTION_PROSPECT_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};