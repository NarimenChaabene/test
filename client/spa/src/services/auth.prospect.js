import axios from "axios";
import {API_URI} from '../constants/apiConstants'


const preinscription = (nom,prenom, email, telephone, Message) => {
    return axios.post(API_URI + "prospect", {
      nom,
      prenom,
      email,
      Message,
      telephone,
    });
  };


  export default {
    preinscription
  };