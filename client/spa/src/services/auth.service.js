import makeToast from "../components/Toaster";
import axios from "axios";
const API_URL = "http://localhost:4000/api/user/";


const register = (nom,prenom, email, password, role) => {
  return axios.post(API_URL + "register", {
    nom,
    prenom,
    email,
    password,
    role,
  });
};

const login =(email, password) => {
     return axios
       .post(API_URL+ "login", {email, password })
      .then((response) => {
        
        
         if (response.data) {
           localStorage.setItem("user", JSON.stringify(response.data));
           makeToast("success");
         }
  
         return response.data;
      });
        
    }
  
 

const logout = () => {
  localStorage.removeItem("user");
};

export default {
  register,
  login,
  logout,
};