import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import imageprofile from '../../images/img-3.jpg'
import './EditProfile.css'

import {updateProfile} from '../../redux/actions/EditProfile'


const EditProfileClient = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const { id } = useParams();
    const { detailsProfile: details } = useSelector((state) => state.profile);

    const [loading, setLoading] = useState(false)
    const [Profile, setProfile] = useState(details);
    const [loadingUpdate, setLoadingUpdate] = useState(false);
    const [errorUpdate, setErrorUpdate] = useState("");
    
    const handleChange = (e) => {
        e.preventDefault()
        const name = e.target.name;
        const value = e.target.value;
        setErrorUpdate("")
        setProfile({ ...Profile, [name]: value });
      };

     
    
      useEffect(() => {
        if (details && Object.keys(details).length > 0) setProfile(details);
      }, [details]);


      const update = async (e) => {
        e.preventDefault();
        setLoadingUpdate(true);
        if (!Profile.nom|| !Profile.prenom || !Profile.email
            || !Profile.password) {
          setErrorUpdate("* Veuillez remplir tous les champs !!");
          setLoadingUpdate(false);
        } else {
          try {
            const newProfile = await dispatch(
              updateProfile( Profile)
            );
            if (newProfile) {
              setLoadingUpdate(false);
              history.push("/editProfile");
              setErrorUpdate("");
            }
          } catch (error) {
            setErrorUpdate("Erreur de survenue");
            setLoadingUpdate(false);
          }
        }
      };
  
  
  
  
    //uploard image 
    const [image, setImage] = useState('')
    const uploadImage = async e => {
      const files = e.target.files
      const data = new FormData()
      data.append('file', files[0])
      data.append('upload_preset', 'darwin')
      setLoading(true)
    

  
      setImage(files)
      setLoading(false)
    }
    return (
      
        
        <div >
            
            <div className="body">
            <div className="container-profile">
            <div className="row gutters">
            <div className="col-xl-3 col-lg-3 col-md-12 col-l-3 col-12">
                <div className="card-profile">
                    <div className="card-body">
                        <div className="account-settings">
                            <div className="user-profile">
                                <div className="user-avatar">
                                {loading ? (
                                    <h3>Loading...</h3>
                                ) : (
                                <img className="img-profile"
                                src={imageprofile}  />
                                )}
                                
                        <div className="image-file">
                        <input type="file" name="" 
                        id="file" 
                        onChange={uploadImage}
                        accept="image/png, image/gif, image/jpeg"/>
                        <label  className="label-pic" for="file"> Modifier la photo </label>
                        </div>
 
                                </div>

                                <div className="name-email">
                                <h5 class="user-name">Narimen Chaabene</h5>
				                <h6 class="user-email">narimenchaabene@gmail.com</h6>
                                </div>

                            </div>
                            
                        </div>
                    </div>
            </div>
            </div>
            <div className="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div className="card-profile">
                    <div className="card-body">
                        <div className="row-gutters">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h6 className="personal-detail">Personal Details</h6>
                            </div>


                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 edit">
                                <div className="form-group">

                                <input className="text-nom" 
                                type="text" 
                                name="" 
                                placeholder=" Nom"
                                value={Profile?.nom}
                                 onChange={handleChange}/>
                                </div>

                                <div className="form-group">

                                <input className="text-prenom"
                                 type="text" 
                                name="" 
                                placeholder="Prenom"
                                value={Profile?.prenom}
                                 onChange={handleChange}/>
                                </div>

                                <div className="form-group">

                                <input className="text-email" 
                                type="email" 
                                name="" 
                                placeholder=" Email"
                                value={Profile?.email}
                                 onChange={handleChange}/>
                                </div>


                                <div className="form-group">

                                <input className="text-password" 
                                type="password" 
                                name="" 
                                placeholder=" Mot de passe"
                                value={Profile?.password}
                                 onChange={handleChange}/>
                                </div>

                                {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Sauvegarder
              </button>
            </div>


                    

                            </div>


                 



                


                        </div>
                    </div>

                </div>
            </div>



            </div>
            </div>
        </div>
        </div>

          
        
    )
}

export default EditProfileClient
