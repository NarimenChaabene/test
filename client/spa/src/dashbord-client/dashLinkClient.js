import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
   
  } from "react-router-dom";



import DashboardClient from './DashboardClient'
import SideDash from './SideDash'

import Feedback from './feedback/Feedback'
import reservation from './reservation/Reservation'
import seanceClient from './seance/SeanceClient'
import EditProfileClient from './EditProfile/EditProfileClient';

import UpdateSeance from './seance/UpdateSeance';




function dashLinkClient() {
    return (
        <div>
            <Router>
                
                <SideDash/>
                <div className="main-card">
                
                    
                <Switch>
                    <Route path='/dashboard' component={DashboardClient}/>
                    <Route path='/editProfileClient' component={EditProfileClient}/>
                    <Route path='/seanceClient'component={seanceClient} />
                    <Route path='/Feedback'component={Feedback} />
                    <Route path='/reservation'component={reservation} />

                    <Route exact path='/updateSeance/:id'component={UpdateSeance} />
             
                </Switch>
                </div>
                
            </Router>
            
        </div>
    )
}

export default dashLinkClient
