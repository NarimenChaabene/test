import React from 'react'
import logo from '../images/logo-dash.png'

import user2 from '../images/user2.jpg'
import {Link} from 'react-router-dom'
import './Dashboard.css'


function DashboardClient() {
    return (
        <div>
            <div className="sidebar-dash">
                <div className="titre-side">
                   <span> <img src={logo} className="logo-side"/></span>

                </div>

                <div className="sidebarDash-menu">
                    
                    <ul className="ul-dash">
                        <li className="titre-menu">
                            <Link to="/dashClient" className="active">
                                
                                <i class="fas fa-home icone-dash"></i>
                                <span className="menu-sidebar"> Dashboard </span>
                            </Link>
                            
                        </li>

                        <li className="titre-menu">
                            <Link to ='/editProfileClient'><i class="fas fa-user-circle icone-dash"></i>
                            <span className="menu-sidebar"> Mon Profile </span> </Link>

                        </li>

                        <li className="titre-menu">
                            <Link to ='/Reservation'> <i class="fas fa-bookmark icone-dash"></i>
                            <span className="menu-sidebar"> Reservation </span> </Link>


                        </li>

                        <li className="titre-menu">
                            <Link to ='/seanceClient'><i class="far fa-list-alt icone-dash"></i>
                            <span className="menu-sidebar"> Seance </span> </Link>


                        </li>


                        <li className="titre-menu">
                            <Link to='/chat'> <i class="fab fa-facebook-messenger icone-dash"></i>
                            <span className="menu-sidebar"> Messagerie </span> </Link>

                        </li>



                        <li className="titre-menu">
                            <Link to ="/Feedback"><i class="fas fa-comments icone-dash"></i>
                            <span className="menu-sidebar"> Feedback </span>
                             </Link>

                        </li>


                        <li className="titre-menu">
                            <Link to =""> <i class="fas fa-sign-out-alt icone-dash"></i>
                            <span className="menu-sidebar"> Se déconnecter </span> 
                            </Link>


                        </li>





                    </ul>

                </div>

                
            </div>


<div className="main">
            <div className="header-dashboard">
                <div className="header-dash">
                    <label for="nav-dash-toggle">
                            <span> <i class="fas fa-bars bars"></i> </span>
                    </label>

                    <div className="search-wrapper">
                        <span>  <i class="fas fa-search"></i>  </span> 
                        <input type="search" placeholder="search-here"/>

                        
                    </div>
                </div>

            </div>

            <div className="main-cards">
                
                <div className="recent-grid">
                        <div className="project">
                            <div className="card">
                                <div className="card-header">
                                    <h2> liste des reservations </h2>
                                   <span> <i className="far fa-list-alt"></i> </span>
                                </div>

                                <div className="card-body">
                                 < div className="table-responsive">
                                    <table width="100%">
                                        <thead className="thead-dash">
                                            <tr className="tr-dash-titre">
                                                <td className="td-dash">Services</td>
                                                <td className="td-dash">Réservation</td>
                                                <td className="td-dash">Status</td>
                                            </tr>

                                        </thead>

                                        <tbody className="tbody-dash">
                                            <tr className="tr-dash">
                                                <td>Massage body</td>
                                                <td>5</td>
                                                <td>
                                                    <span className="status-burly"></span>
                                                        Complet 
                                                </td>
                                            </tr>


                                            <tr className="tr-dash">
                                                <td>SPA & Facial</td>
                                                <td>3</td>
                                                <td>
                                                    <span className="status-burly"></span>
                                                       in progress 
                                                </td>
                                            </tr>



                                            <tr className="tr-dash">
                                                <td>Comp refreshment</td>
                                                <td>3</td>
                                                <td>
                                                    <span className="status-burly"></span>
                                                       in progress 
                                                </td>
                                            </tr>


                                            <tr className="tr-dash">
                                                <td> Massage Therapy</td>
                                                <td>6</td>
                                                <td>
                                                    <span className="status-burly"></span>
                                                       in progress 
                                                </td>
                                            </tr>


                                            <tr className="tr-dash">
                                                <td> Skin SPE Care</td>
                                                <td>6</td>
                                                <td>
                                                    <span className="status-burly"></span>
                                                       in progress 
                                                </td>
                                            </tr>


                                        </tbody>
                                        
                                    </table>
                                </div>
                                </div>

                            </div>

                     </div>




                     {/*...table-Client*/}


                     <div className="new-customers">

                                <div className="card">
                                    <div className="card-header">
                                        <h3>Noveaux Clients </h3>
                                        <button className="btn-card"> Consulter<span><i class="fas fa-user"></i> 
                                        </span> </button>
                                       
                                    </div>

                                    <div className="card-body">
                                    <div className="customer">
                                            <div className="info">
                                            <img src={user2} width={40} height={40}/>
                                            
                                            
                                            <div>
                                                <h4>Lwis Doe</h4>
                                                
                                            </div>
                                            </div>
                                        
                                        <div className="contact"> 
                                            <span> <i class="fas fa-user-circle"></i></span>
                                            <span > <i class="fas fa-comment"></i> </span>
                                            <span > <i class="fas fa-phone"></i></span>
                                        </div>
                                        </div>




                                        <div className="customer">
                                            <div className="info">
                                            <img src={user2} width={40} height={40}/>
                                            
                                            
                                            <div>
                                                <h4>Lwis Doe</h4>
                                                
                                            </div>
                                            </div>
                                        
                                        <div className="contat">
                                            <span> <i class="fas fa-user-circle"></i></span>
                                            <span > <i class="fas fa-comment"></i> </span>
                                            <span > <i class="fas fa-phone"></i></span>
                                        </div>
                                        </div>



                                        <div className="customer">
                                            <div className="info">
                                            <img src={user2} width={40} height={40}/>
                                            
                                            
                                            <div>
                                                <h4>Lwis Doe</h4>
                                                
                                            </div>
                                            </div>
                                        
                                        <div className="contact">
                                            <span> <i class="fas fa-user-circle"></i></span>
                                            <span > <i class="fas fa-comment"></i> </span>
                                            <span > <i class="fas fa-phone"></i></span>
                                        </div>
                                        </div>



                                    </div>

                                    </div>
                                </div>
                            </div>

                        
               
            </div>
                
                


            
            
            
            
           
     </div>
     </div>
     

        
    )
}

export default DashboardClient
