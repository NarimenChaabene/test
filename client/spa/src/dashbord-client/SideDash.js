import React ,{useState} from 'react'
import {Link} from 'react-router-dom'

import user from '../images/user.jpg'
import logo from '../images/logo-dash.png'

//import Dashboard from './Dashboard'
import './SideDash.css'
import { useDispatch } from 'react-redux'
import { logout } from '../redux/actions/auth'





function SideDash  ()  {
    const dispatch = useDispatch();
    const logOut = () => {
        dispatch(logout());
      };
    
    
    return (
        <div>
             <div className="sidebar-dash">
                <div className="titre-side">
                   <span> <img src={logo} className="logo-side"/></span>

                </div>

                <div className="sidebarDash-menu">
                    
                <ul className="ul-dash">
                        <li className="titre-menu">
                            <Link to="/dashboardClient" className="active">
                                
                                <i class="fas fa-home icone-dash"></i>
                                <span className="menu-sidebar"> Dashboard </span>
                            </Link>
                            
                        </li>

                        <li className="titre-menu">
                            <Link to ='/editProfileClient'><i class="fas fa-user-circle icone-dash"></i>
                            <span className="menu-sidebar"> Mon Profile </span> </Link>

                        </li>

                        <li className="titre-menu">
                            <Link to ='/Reservation'><i class="fas fa-bookmark icone-dash"></i>
                            <span className="menu-sidebar"> Reservation </span> </Link>


                        </li>

                        <li className="titre-menu">
                            <Link to ='/seanceClient'><i class="far fa-list-alt icone-dash"></i>
                            <span className="menu-sidebar"> Seance </span> </Link>


                        </li>


                        <li className="titre-menu">
                            <Link to='/chat'><i class="fab fa-facebook-messenger icone-dash"></i>
                            <span className="menu-sidebar"> Messagerie </span> </Link>

                        </li>



                        <li className="titre-menu">
                            <Link to ="/Feedback"><i class="fas fa-comments icone-dash"></i>
                            <span className="menu-sidebar"> Feedback </span>
                             </Link>

                        </li>


                        <li className="titre-menu">
                            <Link to =""> <i class="fas fa-sign-out-alt icone-dash"></i>
                            <span className="menu-sidebar"> Se déconnecter </span> 
                            </Link>


                        </li>





                    </ul>
                </div>

                
            </div>


        <div className="main">
            <div className="header-dashboard">
                <div className="header-dash">
                    <label for="nav-dash-toggle">
                            <span> <i class="fas fa-bars bars"></i> </span>
                    </label>

                    <div className="search-wrapper">
                        <span>  <i class="fas fa-search"></i>  </span> 
                        <input type="search" placeholder="search-here"/>

                        
                    </div>


                
                    





                </div>

            </div>

        </div>

                     
            
            

            
    </div>  
          
        
        
          )
    }
      
      export default SideDash