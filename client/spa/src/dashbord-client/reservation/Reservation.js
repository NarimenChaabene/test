import React, {useState} from 'react'
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css';
import moment from 'moment'
import {Logger, ConsoleLogger} from 'react-console-logger';
import './Reservation.css'

export default function MyReservation() {
  const myLogger = new Logger();
  const [dateState, setDateState] = useState(new Date())
  const changeDate = (e) => {
    setDateState(e)
    console.log('rien a affiché pour : ' );
  }
  return (
    <div className="calander-container">
      <Calendar 
      value={dateState}
      onChange={changeDate}
      />

    <ConsoleLogger
      logger={myLogger}
    /> 
     
      
    <p>Current selected date is <b>{moment(dateState).format('MMMM Do YYYY')}</b></p>
    </div>
 
   
  )

  
}



