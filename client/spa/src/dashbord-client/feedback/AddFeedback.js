import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import { isPassword, isEmail } from "../../helpers/utils";
import "./Feedback.css";
import { addFeedback } from "../../redux/actions/kine";

function AddKine() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [feedback, setFeddback] = useState({
    message: "",
    rating: "",
    
  });
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setErrorAdd("");
    setFeddback({ ...feedback, [name]: value });
  };

  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (
      !feedback.message ||
      !feedback.rating
     
    ) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        
        const newFeedback = await dispatch(addFeedback(feedback, staticToken));
        if (newFeedback) {
          setLoadingAdd(false);
          history.push("/feedbackClient");
          setErrorAdd("");
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue");
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Message</label>
              <input
                type="text"
                class="form-control"
                placeholder="message ..."
                name="message"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">rating</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                onChange={handleChange}
              />
            </div>
           
            {errorAdd && (
              <div className="alert alert-danger" role="alert">
                {errorAdd}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Add feedback
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddKine;
