import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { staticToken } from "../../config";
import { deleteSeance, getAllSeances } from "../../redux/actions/seance";
import moment from "moment"
import "./Seance.css";
import { convertDate } from "../../helpers/utils";
import CustomDialog from "../../components/dialog/CustomDialog";


export default function SeanceClient() {
  const dispatch = useDispatch();
  const history = useHistory();

  const [listSeances, setListSeances] = useState([]);
  const [deleteModal, setDeleteModal] = useState(false);
  const [idToDelete, setIdToDelete] = useState("");
  const [loadingSpinner, setLoadingSpinner] = useState(false);
  const { allSeances, loading, success, failure } = useSelector(
    (state) => state?.seance
  );
  const { user } = useSelector((state) => state?.auth);

  //ANCHOR: fix token of connected user

  useEffect(() => {
    dispatch(getAllSeances(staticToken));
  }, [dispatch, getAllSeances, user]);

  useEffect(() => {
    if (allSeances?.length) {
      setListSeances(allSeances);
    }
  }, [allSeances]);

  const handleEdit = (seance) => {
    if (seance) history.push(`/updateSeance/${seance}`);
  };

  const DeleteModalBody = () => {
    return (
      <>
        <div className="modal-body">
          <p className="text-center">
            Êtes-vous sûr de vouloir supprimer cette Séance ?
          </p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-primary m-0"
            onClick={() => handleRemove(idToDelete)}
          >
            {loadingSpinner && (
              <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            )}
            Confirmer
          </button>
        </div>
      </>
    );
  };

  const handleRemove = async (id) => {
    setLoadingSpinner(true);
    await dispatch(deleteSeance(id, staticToken));
    setLoadingSpinner(false);
    setDeleteModal(null);
  };
  return (
   
    <div>
      <div className="header-seance">
        <h3 className="titre-seance">Gestion des Séances</h3>
        <Link to="/addSeance" className="add-seance btn">
          {" "}
          Ajouter une Séance
        </Link>
      </div>

      {loading && !listSeances && (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}

      <section className="section-seance">
        <div className="table-section-res">
          <table className="seance-table" width="70%">
            <thead className="thead-seance">
              <tr className="tr-seance">
                <td className="td-seance">#</td>
                <td className="td-seance">Description</td>
                <td className="td-seance">Prix</td>
                <td className="td-seance">Date-debut</td>
                <td className="td-seance">Date-fin</td>
                <td className="td-seance">N° Place Max</td>
                <td className="td-seance">N° Reservation</td>
                <td className="td-seance">Actions</td>
              </tr>
            </thead>

            <tbody className="tbody-seance">
              {!loading &&
                success &&
                listSeances?.map((seance, index) => {
                  return (
                    <tr className="tr-seance" key={seance._id}>
                      <td className="td-seance">{index + 1}</td>
                      <td className="td-seance">{seance.Description || "-"}</td>
                      <td className="td-seance">{seance.prix || "-"}</td>
                      
                      <td className="td-seance">
                        <div className="dateDeb-res">{convertDate(seance.dateDebut) || "-"}</div>
                      </td>

                      <td className="td-res">
                        <div className="dateFin-res">{convertDate(seance.dateFin) || "-"}</div>
                      </td>
                      <td className="td-seance">{seance.nbr_Place_Max || "-"}</td>
                      <td className="td-seance">{seance.nbr_Reservation || "-"}</td>
                      <td className="icon-seance">
                        <button
                          className="bg-transparent border-0"
                          onClick={() => handleEdit(seance._id)}
                        >
                          <i className="fas fa-edit edit"></i>
                        </button>
                        <button
                          className="bg-transparent border-0"
                          onClick={() => {
                            setIdToDelete(seance._id);
                            setDeleteModal(true);
                          }}
                        >
                          <i className="fas fa-trash-alt delete"></i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </section>
       <CustomDialog
        show={deleteModal}
        handleClose={() => setDeleteModal(null)}
        header="Supprimer une Séance"
        body={<DeleteModalBody />}
      />
    </div>
  );
}
