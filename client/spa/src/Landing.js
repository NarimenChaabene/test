import React from 'react'

import {BrowserRouter as Router , Route , Switch}
 from 'react-router-dom'
import './App.css'

import Navbar from './components/Landing/Navbar/Navbar'
import ImageSlider from './components/Landing/Carousel/ImageSlider'
import Services from './components/Landing/Services/Services'
import Gallery from './components/Landing/Gallery/Gallery'
import Therapistes from './components/Landing/Therapist/Therapistes'
import Mission from './components/Landing/Mission/Mission'
import Contact from './components/Landing/ContactUs/Contact'
import Footer from './components/Landing/Footer/Footer'
//import Count from './components/Landing/Count/Count'


function Landing() {
  return (
    <Router>
     
            <Navbar/>
      
            <ImageSlider />
            <Services />  
            <Gallery />
            <Therapistes />
            <Mission />
            {/*<Count/>*/}
            <Contact />     
            <Footer />
          
    
    </Router>

  )
}

export default Landing
