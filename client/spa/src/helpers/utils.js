import moment from "moment";

export const isPassword = (password) => {
  if (password.length < 8) {
    return false;
  }
  return true;
};

export const isEmail = (email) => {
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!regex.test(email)) {
    return false;
  }
  return true;
};

export const convertDate = (date) => {
  return moment(date).format("DD-MM-YYYY HH:mm");
};
