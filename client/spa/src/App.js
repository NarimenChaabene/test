import React, { useState, Suspense, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";


import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import { createStore } from "redux";
import { Provider } from "react-redux";
import allReducers from "./redux";


import Login from "./components/login/Login";

import { logout } from "./redux/actions/auth";
import { clearMessage } from "./redux/actions/message";

import { history } from "./helpers/history";

//////////
import Landing from "./Landing";
import dashboard from "./dashbboard-Admin/dashLink";
import Categorie from "./dashbboard-Admin/Categories/Categorie";
import register from "./components/register/Register";
import preInscription from './components/pre_inscription/PreInscription'
import seanceClient from './dashbord-client/seance/SeanceClient'
import dashboardClient from './dashbord-client/dashLinkClient'
import reservation from './dashbord-client/reservation/Reservation'


function App() {
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    history.listen((location) => {
      dispatch(clearMessage()); // clear message when changing location
    });
  }, [dispatch]);

  const logOut = () => {
    dispatch(logout());
  };

  return (
    <div className="App">
      <dashLink />
      <Router>
        <Route path="/" exact component={Landing} />
        <Route path="/dashboard" component={dashboard} />
        <Route path="/categorie" component={Categorie} />
        <Route path="/preinscription" component={preInscription} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={register} />
       
        <Route exact path="/dashboardClient" component={dashboardClient} />
        <Route exact path="/seanceclient" component={seanceClient} />
  <Route exact path="/reservation" component={reservation} />
        

      </Router>
    </div>
  );
}

export default App;
