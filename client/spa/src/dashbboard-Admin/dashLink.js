import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
   
  } from "react-router-dom";
import EditProfile from './EditProfile/EditProfile';
import Membres from './Membre/Membres'
import Categorie from './Categories/Categorie'
import kinesitherapeute from './Kinésithérapeute/Kinesitherapeute'
import Reservation from './Reservation/Reservation'
import Seance from './Seances/Seance'
import Prospect from './Prospect/Prospect'
import Feedback from './Feedback/Feedback'
import Dashboard from './Dashboard'

import SideDash from './SideDash'
import AddCategorie from './Categories/AddCategorie';
import UpdateCategorie from './Categories/UpdateCategorie';
import UpdateMembre from './Membre/UpdateMembre';
import AddMembre from './Membre/AddMembre';
import AddKine from './Kinésithérapeute/AddKine';
import UpdateKine from './Kinésithérapeute/UpdateKine';
import AddSeance from './Seances/AddSeance';
import UpdateSeance from './Seances/UpdateSeance';
import AddReservation from './Reservation/AddReservation';
import UpdateReservation from './Reservation/UpdateReservation';
import AddProspect from './Prospect/AddProspect';
import UpdateProspect from './Prospect/UpdateProspect';

function dashLink() {
    return (
        <div>
            <Router>
                
                <SideDash/>
                <div className="main-card">
                
                    
                <Switch>
                    <Route path='/dashboard' component={Dashboard}/>
                    <Route path='/editProfile' component={EditProfile}/>
                    <Route path='/membres'component={Membres} />
                    <Route path='/prospect'component={Prospect} />
                    <Route exact path='/categorie'component={Categorie} />
                    <Route exact path='/kinesitherapeute'component={kinesitherapeute} />
                    <Route path='/reservation'component={Reservation} />
                    <Route path='/seance'component={Seance} />
                    <Route path='/feedback'component={Feedback} />
                    {/* <Route path='/categorie/add'component={AddCategorie} /> */}
                    <Route exact path='/addCategorie'component={AddCategorie} />
                    <Route exact path='/updateCategorie/:id'component={UpdateCategorie} />
                    <Route exact path='/updateMembre/:id'component={UpdateMembre} />
                    <Route exact path='/addMembre'component={AddMembre} />
                    <Route exact path='/addKine'component={AddKine} />
                    <Route exact path='/updateKine/:id'component={UpdateKine} />
                    <Route exact path='/addSeance'component={AddSeance} />
                    <Route exact path='/updateSeance/:id'component={UpdateSeance} />
                    <Route exact path='/addReservation'component={AddReservation} />
                    <Route exact path='/updateReservation/:id'component={UpdateReservation} />
                    <Route exact path='/addProspect'component={AddProspect} />
                    <Route exact path='/updateProspect/:id'component={UpdateProspect} />
                </Switch>
                </div>
                
            </Router>
            
        </div>
    )
}

export default dashLink
