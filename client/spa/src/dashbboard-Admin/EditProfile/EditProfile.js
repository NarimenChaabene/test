import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import "./EditProfile.css";
import {  updateProfile } from "../../redux/actions/EditProfile";
import imageprofile from '../../images/img-3.jpg'
import './EditProfile.css'


const EditProfile = () => {
    const [image, setImage] = useState('')
    const [loading, setLoading] = useState(false)
  
    const uploadImage = async e => {
      const files = e.target.files
      const data = new FormData()
      data.append('file', files[0])
      data.append('upload_preset', 'darwin')
      setLoading(true)
    

  
      setImage(files)
      setLoading(false)
    }


    const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  
 

  const { detailsProfile: details } = useSelector((state) => state.editProfile);
  const [editProfile, setEditProfile] = useState();
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");

  
  const handleChange = (e) => {
    e.preventDefault()
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("")
    setEditProfile({ ...editProfile, [name]: value });
  };


  useEffect(() => {
    if (details && Object.keys(details).length > 0) setEditProfile(details);
  }, [details]);

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (!editProfile.nom|| !editProfile.prenom || !editProfile.email || !editProfile.password) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        const newProfile = await dispatch(
          updateProfile(id, editProfile)
        );
        if (newProfile) {
          setLoadingUpdate(false);
          history.push("/editProfileClient");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

    return (
      
        
        <div >
            
            <div className="body">
            <div className="container-profile">
            <div className="row gutters">
            <div className="col-xl-3 col-lg-3 col-md-12 col-l-3 col-12">
                <div className="card-profile">
                    <div className="card-body">
                        <div className="account-settings">
                            <div className="user-profile">
                                <div className="user-avatar">
                                {loading ? (
                                    <h3>Loading...</h3>
                                ) : (
                                <img className="img-profile"
                                src={imageprofile}  />
                                )}
                                
                        <div className="image-file">
                        <input type="file" name="" 
                        id="file" 
                        onChange={uploadImage}
                        accept="image/png, image/gif, image/jpeg"/>
                        <label  className="label-pic" for="file"> Modifier la photo </label>
                        </div>
 
                                </div>

                                <div className="name-email">
                                <h5 class="user-name">Yuki Hayashi</h5>
				                <h6 class="user-email">yuki@gmail.com</h6>
                                </div>

                            </div>
                            <div className="about">
                            <h5 className="about-card">About</h5>
                            <p>I'm Yuki. administrateur du SPA THERME</p>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
            <div className="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div className="card-profile">
                    <div className="card-body">
                        <div className="row-gutters">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h6 className="personal-detail">Personal Details</h6>
                            </div>


                            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 edit">
                                <div className="form-group">
                                <input className="text-nom" type="text" 
                                name="" placeholder=" Nom"
                                value={editProfile?.nom}
                                onChange={handleChange}/>
                                </div>


                                <div className="form-group">
                                <input className="text-prenom" type="text" 
                                name="" placeholder="Prenom"
                                
                                value={editProfile?.prenom}
                                onChange={handleChange}/>
                                </div>

                                <div className="form-group">
                                <input className="text-email" type="email" 
                                name="" placeholder=" Email"
                                value={editProfile?.email}
                                onChange={handleChange}/>
                                </div>


                                <div className="form-group">
                                <input className="text-password" type="password" 
                                name="" placeholder=" Mot de passe"
                                value={editProfile?.password}
                                 onChange={handleChange}/>
                                </div>


                                {/*<div className="form-group">
                                <input className="text-password2" type="password" 
                                name="" placeholder=" Confirmer votre mot de passe"/>
                                </div>*/}

                            </div>


                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <div className="text-right">
                                <button type="button" id="submit" name="submit" className="cancel">Annuler</button>
                                <button
                className="done"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Sauvegarder
              </button>
                                </div>
                            </div>



                


                        </div>
                    </div>

                </div>
            </div>



            </div>
            </div>
        </div>
        </div>

          
        
    )
}

export default EditProfile
