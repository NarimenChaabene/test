import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import "./EditProfile.css";
import {  updateProfile } from "../../redux/actions/EditProfile";

function UpdateProfile() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  
  


  const [editProfile, setEditProfile] = useState(details);
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");
  const { detailsProfile: details } = useSelector((state) => state.editProfile);


  const handleChange = (e) => {
    e.preventDefault()
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("")
    setEditProfile({ ...editProfile, [name]: value });
  };


 /* useEffect(() => {
    if (details && Object.keys(details).length > 0) setEditProfile(details);
  }, [details]);*/

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (!editProfile.nom|| !editProfile.prenom || !editProfile.email || !editProfile.password) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        const newProfile = await dispatch(
          updateProfile(id, editProfile)
        );
        if (newProfile) {
          setLoadingUpdate(false);
          history.push("/editProfileClient");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                value={editProfile?.nom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="prenom ..."
                name="prenom"
                value={editProfile?.prenom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">email</label>
              <input
                type="text"
                class="form-control"
                placeholder="email..."
                name="email"
                value={editProfile?.email}
                onChange={handleChange}
              />
            </div>

            <div class="form-group my-3">
              <label className="py-2">Password</label>
              <input
                type="text"
                class="form-control"
                placeholder="Titre ..."
                name="Titre"
                value={editProfile?.prenom}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update categorie
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateProfile;
