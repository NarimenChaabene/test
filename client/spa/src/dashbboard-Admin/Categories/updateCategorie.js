import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import "./Categorie.css";
import { getCategorie, updateCategorie } from "../../redux/actions/categorie";

function UpdateCategorie() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  
  const { detailsCategorie: details } = useSelector((state) => state.categorie);


  const [categorie, setCategorie] = useState(details);
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");


  const handleChange = (e) => {
    e.preventDefault()
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("")
    setCategorie({ ...categorie, [name]: value });
  };
  useEffect(() => {
    dispatch(getCategorie(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setCategorie(details);
  }, [details]);

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (!categorie.Titre || !categorie.Description) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        const newCategorie = await dispatch(
          updateCategorie(id, categorie)
        );
        if (newCategorie) {
          setLoadingUpdate(false);
          history.push("/categorie");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                value={categorie?.Description}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Titre</label>
              <input
                type="text"
                class="form-control"
                placeholder="Titre ..."
                name="Titre"
                value={categorie?.Titre}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update categorie
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateCategorie;
