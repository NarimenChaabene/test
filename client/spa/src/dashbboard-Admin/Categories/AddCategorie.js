import React, { useState } from "react";

import { addCategorie } from "../../redux/actions/categorie";
import "./Categorie.css";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";

function AddCategorie() {
  const dispatch = useDispatch();
  const history = useHistory()
  const [Description, setDescription] = useState("");
  const [Titre, setTitre] = useState("");
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (!Titre || !Description) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        const newCategorie = await dispatch(
          addCategorie(Description, Titre, staticToken)
        );
        if (newCategorie) {
          setLoadingAdd(false);
          history.push("/categorie");
          setErrorAdd("");
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue")
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                onChange={(event) => {
                  setDescription(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Titre</label>
              <input
                type="text"
                class="form-control"
                placeholder="Titre ..."
                name="Titre"
                onChange={(event) => {
                  setTitre(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>
           {errorAdd &&  <div className="alert alert-danger" role="alert">
              {errorAdd}
            </div>}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Add categorie
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddCategorie;
