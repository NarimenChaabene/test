import React, { useState } from "react";

import { addProspect } from "../../redux/actions/prospect";
import "./Prospect.css";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import { isEmail } from "../../helpers/utils";

function AddProspect() {
  const dispatch = useDispatch();
  const history = useHistory()
  const [nom, setNom] = useState("");
  const [prenom, setPrenom] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");
  const [Message, setMessage] = useState("");
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (!nom || !prenom || !email || !telephone || !Message) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        if (!isEmail(email)) {
          setErrorAdd("Entrer un email valide !!");
          setLoadingAdd(false)
          return;
        }
        const newProspect = await dispatch(
          addProspect(nom, prenom,email,telephone , Message, staticToken)
        );
        if (newProspect) {
          setLoadingAdd(false);
          history.push("/prospect");
          setErrorAdd("");
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue")
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="Nom"
                onChange={(event) => {
                  setNom(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="Prenom"
                onChange={(event) => {
                  setPrenom(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>


            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Email ..."
                name="Email"
                onChange={(event) => {
                  setEmail(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>

            <div class="form-group my-3">
              <label className="py-2">Telephone</label>
              <input
                type="number"
                class="form-control"
                placeholder="Telephone ..."
                name="Telephone"
                onChange={(event) => {
                  setTelephone(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>

            <div class="form-group my-3">
              <label className="py-2">Message</label>
              <input
                type="text"
                class="form-control"
                placeholder="Message ..."
                name="Message"
                onChange={(event) => {
                  setMessage(event.target.value);
                  setErrorAdd("");
                }}
              />
            </div>
           {errorAdd &&  <div className="alert alert-danger" role="alert">
              {errorAdd}
            </div>}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Add Prospect
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddProspect;
