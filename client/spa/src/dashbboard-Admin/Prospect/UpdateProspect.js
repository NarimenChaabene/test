import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import "./Prospect.css";
import { getProspect, updateProspect} from "../../redux/actions/prospect";
import { isEmail } from "../../helpers/utils";

function UpdateProspect() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  
  const { detailsProspect: details } = useSelector((state) => state?.prospect);


  const [prospect, setProspect] = useState(details);
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");


  const handleChange = (e) => {
    // e.preventDefault()
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("")
    setProspect({ ...prospect, [name]: value });
  };
  useEffect(() => {
    dispatch(getProspect(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setProspect(details);
  }, [details]);

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (!prospect.nom || !prospect.prenom || !prospect.email || !prospect.telephone || !prospect.Message) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        if (!isEmail(prospect.email)) {
          setErrorUpdate("Entrer un email valide !!");
          setLoadingUpdate(false)
          return;
        }
        const newProspect = await dispatch(
          updateProspect(id, prospect)
        );
        if (newProspect) {
          setLoadingUpdate(false);
          history.push("/prospect");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="nom"
                value={prospect?.nom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                value={prospect?.prenom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="text"
                class="form-control"
                placeholder="Email..."
                name="email"
                value={prospect?.email}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Message</label>
              <input
                type="text"
                class="form-control"
                placeholder="Message ..."
                name="Message"
                value={prospect?.Message}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update Prospect
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateProspect;
