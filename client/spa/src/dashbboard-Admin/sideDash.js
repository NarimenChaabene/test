import React ,{useState} from 'react'
import {Link} from 'react-router-dom'

import user from '../images/user.jpg'
import logo from '../images/logo-dash.png'

//import Dashboard from './Dashboard'
import './SideDash.css'
import { useDispatch } from 'react-redux'
import { logout } from '../redux/actions/auth'





function SideDash  ()  {
    const dispatch = useDispatch();
    const logOut = () => {
        dispatch(logout());
      };
    
    
    return (
        <div>
             <div className="sidebar-dash">
                <div className="titre-side">
                   <span> <img src={logo} className="logo-side"/></span>

                </div>

                <div className="sidebarDash-menu">
                    
                    <ul className="ul-dash">
                        <li className="titre-menu">
                            <Link to="/dashboard" className="active">
                                <i class="fas fa-home icone-dash"></i>
                                <span className="menu-sidebar"> Dashboard </span>
                            </Link>
                            
                        </li>

                        <li className="titre-menu">
                            <Link to ='/editProfile'><i class="fas fa-user-circle icone-dash"></i>
                            <span className="menu-sidebar"> Mon Profile </span> </Link>

                        </li>
                        <li className="titre-menu">
                            <Link to ='/membres'><i class="fas fa-users icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des clients </span> </Link>


                        </li>

                        <li className="titre-menu">
                            <Link to ='/prospect'><i class="fas fa-user icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des prospects </span> </Link>


                        </li>


                        <li className="titre-menu">
                            <Link to='/kinesitherapeute'><i class="fas fa-male icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des kinésithérapeutes </span> </Link>

                        </li>


                        <li className="titre-menu">
                            <Link to ='/categorie'>  <i class="fas fa-shapes icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des catégories </span> </Link>
                        
                        </li>




                        <li className="titre-menu">
                            <Link to ="/seance">  <i class="far fa-list-alt icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des séances </span> </Link>


                        </li>

                        <li className="titre-menu">
                            <Link to='/reservation'> <i class="fas fa-bookmark icone-dash"></i>
                            <span className="menu-sidebar"> Gestion des réservations </span> 
                            
                            </Link>

                        </li>

                        <li className="titre-menu">
                            <Link to ="/feedback"><i class="fas fa-comments icone-dash"></i>
                            <span className="menu-sidebar"> Feedback </span>
                             </Link>

                        </li>


                        <li className="titre-menu">
                            <Link to ="" onClick={logOut}> <i class="fas fa-sign-out-alt icone-dash"></i>
                            <span className="menu-sidebar"> Se déconnecter </span> 
                            </Link>


                        </li>





                    </ul>

                </div>

                
            </div>


        <div className="main">
            <div className="header-dashboard">
                <div className="header-dash">
                    <label for="nav-dash-toggle">
                            <span> <i class="fas fa-bars bars"></i> </span>
                    </label>

                    <div className="search-wrapper">
                        <span>  <i class="fas fa-search"></i>  </span> 
                        <input type="search" placeholder="Recherche"/>

                        
                    </div>


                    
                        {/*<div className="name-admin">
                            <h4 className="name-title">Jhon</h4>
                            <small>Administrateur</small>
    </div>*/}
                    





                </div>

            </div>

            </div>

                     
            
            

            
        </div>  
          
        
        
          )
      }
      
      export default SideDash