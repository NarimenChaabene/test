import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import { staticToken } from "../../config";
import {
  deleteReservation,
  getAllReservations,
} from "../../redux/actions/reservation";
import { allReservations } from '../../redux/reducers/reservation';
import CustomDialog from "../../components/dialog/CustomDialog";
import "./Reservation.css";

function Reservation() {
  const dispatch = useDispatch();
  const history = useHistory();

  const [listReservations, setListReservations] = useState([]);
  const [deleteModal, setDeleteModal] = useState(false);
  const [idToDelete, setIdToDelete] = useState("");
  const [loadingSpinner, setLoadingSpinner] = useState(false);
  const { allReservations, loading, success, failure } = useSelector(
    (state) => state?.reservation
  );
  const { user } = useSelector((state) => state?.auth);

  //ANCHOR: fix token of connected user

  useEffect(() => {
    dispatch(getAllReservations(staticToken));
  }, [dispatch, getAllReservations, user]);

  useEffect(() => {
    if (allReservations?.length) {
      setListReservations(allReservations);
    }
  }, [allReservations]);

  const handleEdit = (reservation) => {
    if (reservation) history.push(`/updateReservation/${reservation}`);
  };

  const DeleteModalBody = () => {
    return (
      <>
        <div className="modal-body">
          <p className="text-center">
            Êtes-vous sûr de vouloir supprimer cette Séance ?
          </p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-primary m-0"
            onClick={() => handleRemove(idToDelete)}
          >
            {loadingSpinner && (
              <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            )}
            Confirmer
          </button>
        </div>
      </>
    );
  };

  const handleRemove = async (id) => {
    setLoadingSpinner(true);
    await dispatch(deleteReservation(id, staticToken));
    setLoadingSpinner(false);
    setDeleteModal(null);
  };

  return (
    <div>
      <div className="header-reservation">
        <h3 className="titre-reservation">Gestion des réservations</h3>
        <Link to="/addReservation" className="add-reservation btn">
          {" "}
          Ajouter une reservation
        </Link>
      </div>

      {loading && !listReservations && (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}

      <section className="section-reservation">
        <div className="table-responsive-res">
          <table className="reservation-table" width="70%">
            <thead className="thead-reservation">
              <tr className="tr-cat">
                <td className="td-res">#</td>
                <td className="td-res">Description</td>
                <td className="td-res">Date</td>
                <td className="td-res">Heure</td>
                <td className="td-res">Etat</td>
                <td className="td-res">Actions</td>
              </tr>
            </thead>

            <tbody className="tbody-reservation">
              {!loading &&
                success &&
                listReservations?.map((reservation, index) => {
                  return (
                    <tr className="tr-res" key={reservation._id}>
                      <td className="td-res">{index + 1}</td>
                      <td className="td-res">
                        {reservation.Description || "-"}
                      </td>
                      <td className="td-res">
                        <div className="td-res">
                          {moment(reservation.dateReservation).format(
                            "DD/MM/YYYY"
                          ) || "-"}
                        </div>
                      </td>
                      <td className="td-res">
                        <div className="heure-res">
                          {`${moment(reservation.dateReservation)
                            .utc(reservation.dateReservation)
                            .local()
                            .format("HH:mm")} h` || "-"}
                        </div>
                      </td>

                      <td className="td-res">
                        <div className="etat-res">
                          {reservation.etat || "-"}
                        </div>
                      </td>
                      <td className="icon-res">
                        <button
                          className="bg-transparent border-0"
                          onClick={() => handleEdit(reservation._id)}
                        >
                          <i className="fas fa-edit edit"></i>
                        </button>
                        <button
                          className="bg-transparent border-0"
                          onClick={() => {
                            setIdToDelete(reservation._id);
                            setDeleteModal(true);
                          }}
                        >
                          <i className="fas fa-trash-alt delete"></i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </section>
      <CustomDialog
        show={deleteModal}
        handleClose={() => setDeleteModal(null)}
        header="Supprimer une Réservation"
        body={<DeleteModalBody />}
      />
    </div>
  );
}

export default Reservation;
