import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import DatePicker from "react-flatpickr";
import { getReservation, updateReservation } from "../../redux/actions/reservation";
import "./Reservation.css";

function UpdateReservation() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const { detailsReservation: details } = useSelector((state) => state.reservation);

  const [reservation, setReservation] = useState({});
  const [dateReservation, setDateReservation] = useState(new Date(details?.dateReservation));
  const [Heure, setHeure] = useState(new Date(details?.Heure));
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");


  const handleChange = ({target}) => {
    setErrorUpdate("");
    setReservation({ ...reservation, [target.name]: target.value });
  };
  useEffect(() => {
    dispatch(getReservation(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setReservation(details);
  }, [details]);

  const handleChangeDateRes = (date) => {
    const start = new Date(date);
    setDateReservation(start);
  };
  const handleChangeHeure = (date) => {
    const heure = new Date(date);
    setHeure(heure);
  };

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (
      !reservation.Description ||
      !reservation.Heure 
    ) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        const newreservation = await dispatch(
          updateReservation({
            id,
            data: {
              ...reservation,
              dateReservation: new Date(dateReservation),
              Heure: new Date(Heure),
            },
            token: staticToken,
          })
        );
        if (newreservation) {
          setLoadingUpdate(false);
          history.push("/reservation");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                value={reservation?.Description || "-"}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Reservation</label>
              <DatePicker
                className="form-control"
                value={dateReservation}
                name="dateReservation"
                onChange={(date) =>
                  handleChangeDateRes(date)
                }
                options={{
                  altInput: true,
                  minDate: 'today',
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Heure</label>
              <DatePicker
                className="form-control"
                value={Heure}
                name="Heure"
                onChange={(date) =>{
                  handleChangeHeure(date)
                }
                }
                options={{
                  altInput: true,
                  minDate: 'today',
                  dateFormat: "Y-m-d H:i",
                  enableTime: true,
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Etat</label>
              <input
                type="text"
                class="form-control"
                placeholder="Etat ..."
                name="etat"
                value={reservation?.etat || "-"}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update Reservation
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateReservation;
