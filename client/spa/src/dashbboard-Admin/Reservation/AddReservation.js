import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import DatePicker from "react-flatpickr";
import moment from "moment";
import { addReservation } from "../../redux/actions/reservation";
import "./Reservation.css";

function AddReservation() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [reservation, setReservation] = useState({
    Description: "",
    etat: "",
  });
  const [dateReservation, setDateReservation] = useState(
    moment().utc().local().format("YYYY/MM/DD")
  );
  const [Heure, setHeure] = useState(
    moment().utc().local().format("YYYY/MM/DD hh:mm")
  );
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setErrorAdd("");
    setReservation({ ...reservation, [name]: value });
  };

  const handleChangeDateRes = (date) => {
    const start = moment().utc(date).local().format("YYYY/MM/DD");
    setDateReservation(start);
  };
  const handleChangeHeure = (date) => {
    const heure = moment().utc(date).local().format("YYYY/MM/DD");
    setHeure(heure);
  };

  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (!reservation.Description || !reservation.etat) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        const convertedDate = {
          ...reservation,
          dateReservation: moment(dateReservation),
          Heure: moment(Heure),
        };
        const newReservation = await dispatch(
          addReservation(convertedDate, staticToken)
        );
        if (newReservation) {
          setLoadingAdd(false);
          history.push("/reservation");
          setErrorAdd("");
        } else {
          setErrorAdd("Verifier les dates");
          setLoadingAdd(false);
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue");
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Reservation</label>
              <DatePicker
                className="form-control"
                value={dateReservation}
                onChange={(date) => handleChangeDateRes(date)}
                options={{
                  altInput: true,
                  minDate: 'today',
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Heure</label>
              <DatePicker
                className="form-control"
                value={Heure}
                onChange={(date) => handleChangeHeure(date)}
                options={{
                  altInput: true,
                  minDate: 'today',
                  dateFormat: "Y-m-d H:i",
                  enableTime: true,
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Etat</label>
              <input
                type="text"
                class="form-control"
                placeholder="Etat ..."
                name="etat"
                onChange={handleChange}
              />
            </div>
            {errorAdd && (
              <div className="alert alert-danger" role="alert">
                {errorAdd}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Add Reservation
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddReservation;
