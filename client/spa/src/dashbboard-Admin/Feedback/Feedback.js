import React, { useEffect, useState } from "react";
import "./Feedback.css";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllFeedback,
  deleteFeedback
} from "../../redux/actions/FeedbackAdmin";
import CustomDialog from "../../components/dialog/CustomDialog";
import { Link, useHistory } from "react-router-dom";
import { staticToken } from "../../config";

const Categorie = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [listFeedback, setListFeedback] = useState([]);
  const [deleteModal, setDeleteModal] = useState(false);
  const [idToDelete, setIdToDelete] = useState("");
  const [loadingSpinner, setLoadingSpinner] = useState(false);
  const { allFeedback, loading, success, failure } = useSelector(
    (state) => state?.feedback
  );
  const { user } = useSelector((state) => state?.auth);

  //ANCHOR: fix token of connected user

  useEffect(() => {
    dispatch(getAllFeedback(staticToken));
  }, [dispatch, getAllFeedback, user]);

  const table = () =>{
    const allFeedback =[]
    const message = allFeedback.map((allFeedback))
    const evaluation = allFeedback.map((allFeedback))
  }

  /*function feedbackList(props) {
    const allFeedback = props.allFeedback;
    const message = allFeedback.map((feedback) =>
      <li>{feedback}</li>
     
    );
    const evaluation = allFeedback.map((feedback) =>
    <li>{feedback}</li>);
    return (
      <ul>{feedbackList}</ul>
    );
  }*/

  useEffect(() => {
    if (allFeedback.length) {
      setListFeedback(allFeedback);
      
    }
  }, [allFeedback]);

  const handleEdit = (feedback) => {
      if(feedback) history.push(`/updateFeedback/${feedback}`)
  };

  const DeleteModalBody = () => {
    return (
      <>
        <div className="modal-body">
          <p className="text-center">
            Êtes-vous sûr de vouloir supprimer cette feedback ?
          </p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-primary m-0"
            onClick={() => handleRemove(idToDelete)}
          >
            {loadingSpinner && (
              <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            )}
            Confirmer
          </button>
        </div>
      </>
    );
  };

  const handleRemove = async (id) => {
    setLoadingSpinner(true);
    await dispatch(deleteFeedback(id, staticToken));
    setLoadingSpinner(false);
    setDeleteModal(null);
  };

  return (
    <div className="body-categorie">
      <div className="header-categorie">
        <h3 className="titre-categorie">Gestion des feedbacks</h3>

      </div>
     
      {loading && !listFeedback && (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}

      <section className="section-categorie">
        <div className="table-responsive-cat">
          <table className="categorie-table " width="70%">
            <thead className="thead-categorie">
              <tr className="tr-cat">
                <td className="td-cat"># </td>
                <td className="td-cat">Nom </td>
                <td className="td-cat">Prenom </td>
                <td className="td-cat">Message </td>
                <td className="td-cat">evaluation </td>
                <td className="td-cat">Actions</td>
              </tr>
            </thead>
            <tbody className="tbody-categorie">
              {!loading &&
                success &&
                listFeedback?.map((feedback, index) => {
                  return (
                    <tr className="tr-cat" key={feedback?._id}>
                      <td className="td-cat">{index + 1}</td>
                      <td className="td-cat">{feedback.message || "-"}</td>
                      <td className="td-cat">
                        <p className="desc-cat">
                          {feedback.evaluation || "-"}
                        </p>
                      </td>
                      <td className="icon-cat">
                        <button
                          className="bg-transparent border-0"
                          onClick={() => handleEdit(feedback._id)}
                        >
                          <i className="fas fa-edit edit"></i>
                        </button>
                        <button
                          className="bg-transparent border-0"
                          onClick={() => {
                            setIdToDelete(feedback._id);
                            setDeleteModal(true);
                          }}
                        >
                          <i className="fas fa-trash-alt delete"></i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </section>
      <CustomDialog
        show={deleteModal}
        handleClose={() => setDeleteModal(null)}
        header="Supprimer une Catégorie"
        body={<DeleteModalBody />}
      />
    </div>
  );
};

export default Categorie;
