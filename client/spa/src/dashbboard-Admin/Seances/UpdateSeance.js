import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import { getSeance, updateSeance } from "../../redux/actions/seance";
import DatePicker from "react-flatpickr";
import "./Seance.css";

function UpdateSeance() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const { detailsSeance: details } = useSelector((state) => state.seance);

  const [seance, setSeance] = useState({});
  const [dateDebut, setDateDebut] = useState(new Date(details?.dateDebut));
  const [dateFin, setDateFin] = useState(new Date(details?.dateFin));
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");


  const handleChange = ({target}) => {
    setErrorUpdate("");
    setSeance({ ...seance, [target.name]: target.value });
  };
  useEffect(() => {
    dispatch(getSeance(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setSeance(details);
  }, [details]);

  const handleChangeDateDebut = (date) => {
    const start = new Date(date);
    setDateDebut(start);
  };
  const handleChangeDateFin = (date) => {
    const fin = new Date(date);
    setDateFin(fin);
  };

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (
      !seance.Description ||
      !seance.prix ||
      !seance.nbr_Place_Max ||
      !seance.nbr_Reservation
    ) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        const newSeance = await dispatch(
          updateSeance({
            id,
            data: {
              ...seance,
              dateDebut: new Date(dateDebut),
              dateFin: new Date(dateFin),
            },
            token: staticToken,
          })
        );
        if (newSeance) {
          setLoadingUpdate(false);
          history.push("/seance");
          setErrorUpdate("");
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Prix</label>
              <input
                type="number"
                class="form-control"
                placeholder="Prix ..."
                name="prix"
                value={seance?.prix || "-"}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                value={seance?.Description || "-"}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Debut</label>
              <input
                className="form-control"
                value={dateDebut}
                name="dateDebut"
                onChange={(date) =>
                  handleChangeDateDebut(date)
                  // handleChange({
                  //   target: {
                  //     name: "dateDebut",
                  //     value: date,
                  //   },
                  // })
                }
                options={{
                  altInput: true,
                  minDate: 'today',
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Fin</label>
              <input
                className="form-control"
                value={dateFin}
                name="dateFin"
                onChange={(date) =>{
                  handleChangeDateFin(date)
                  // handleChange({
                  //   target: {
                  //     name: "dateFin",
                  //     value: date,
                  //   },
                  // })
                }
                }
                options={{
                  altInput: true,
                  minDate: 'today',
                  altFormat: "j F, Y",
                  dateFormat: "dd/MM/yyyy",
                }}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">N° Place Max</label>
              <input
                type="number"
                class="form-control"
                placeholder="N° Place Max ..."
                name="nbr_Place_Max"
                value={seance?.nbr_Place_Max || "-"}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">N° Reservation</label>
              <input
                type="number"
                class="form-control"
                placeholder="N° Reservation ..."
                name="nbr_Reservation"
                value={seance?.nbr_Reservation || "-"}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Sauvegarder
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateSeance;
