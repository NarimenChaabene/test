import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import { addSeance } from "../../redux/actions/seance";
import DatePicker from "react-flatpickr";
import moment from "moment";
import "moment/locale/fr";
import "./Seance.css";
import Flatpickr from "react-flatpickr";

function AddSeance() {
  
  const dispatch = useDispatch();
  const history = useHistory();
  const [seance, setSeance] = useState({
    Description: "",
    prix: "",
    nbr_Place_Max: "",
    nbr_Reservation: "",
  });
  const [dateDebut, setDateDebut] = useState(
    moment(new Date()).format("YYYY/MM/DD")
  );
  const [dateFin, setDateFin] = useState(
    moment(new Date()).format("YYYY/MM/DD")
  );
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setErrorAdd("");
    setSeance({ ...seance, [name]: value });
  };

  const handleChangeDateDebut = (date) => {
    const start = moment(date).format("YYYY/MM/DD");
    setDateDebut(start);
  };
  const handleChangeDateFin = (date) => {
    const fin = moment(date).format("YYYY/MM/DD");
    setDateFin(fin);
  };


  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (
      !seance.Description ||
      !seance.prix ||
      !seance.nbr_Place_Max ||
      !seance.nbr_Reservation
      

    ) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        const newtSeance = {...seance, dateDebut: moment(dateDebut),dateFin: moment(dateFin)}
        const newSeance = await dispatch(addSeance(newtSeance, staticToken));
        if (newSeance) {
          setLoadingAdd(false);
          history.push("/seance");
          setErrorAdd("");
        }else{
          setErrorAdd("Verifier les dates");
          setLoadingAdd(false)
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue");
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Prix</label>
              <input
                type="number"
                class="form-control"
                placeholder="Prix ..."
                name="prix"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Description</label>
              <input
                type="text"
                class="form-control"
                placeholder="Description ..."
                name="Description"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Debut</label>
              <input
                type="text"
                
                class="form-control"
                placeholder=""
                name="Description"
                
                onChange={(date) => handleChangeDateDebut(date)}
              />
    
              
             {/*} <DatePicker
                className="form-control"
                value={dateDebut}
                onChange={(date) => handleChangeDateDebut(date)}
                options={{
                  altInput: true,
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
              />*/}
            </div>
            <div class="form-group my-3">
              <label className="py-2">Date Fin</label>
              <input
                type="text"
                
                class="form-control"
                placeholder=""
                name="Description"
                className="form-control"
                
                options={{
                  altInput: true,
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
                onChange={(date) => handleChangeDateFin(date)}
              />
             {/*} <DatePicker
                className="form-control"
                value={dateFin}
                onChange={(date) => handleChangeDateFin(date)}
                options={{
                  altInput: true,
                  altFormat: "F j, Y",
                  dateFormat: "Y-m-d",
                }}
              />*/}
            </div>
            <div class="form-group my-3">
              <label className="py-2">N° Place Max</label>
              <input
                type="number"
                class="form-control"
                placeholder="N° Place Max ..."
                name="nbr_Place_Max"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">N° Reservation</label>
              <input
                type="number"
                class="form-control"
                placeholder="N° Reservation ..."
                name="nbr_Reservation"
                onChange={handleChange}
              />
            </div>
            {errorAdd && (
              <div className="alert alert-danger" role="alert">
                {errorAdd}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto add-seance btn"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Ajouter séance
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddSeance;
