import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import CustomDialog from "../../components/dialog/CustomDialog";
import { staticToken } from "../../config";
import { deleteMembre, getAllMembres } from "../../redux/actions/membre";
import "./Membres.css";

function Membres() {
  const dispatch = useDispatch();
  const history = useHistory();

  const [listMembres, setListMembres] = useState([]);
  const [deleteModal, setDeleteModal] = useState(false);
  const [idToDelete, setIdToDelete] = useState("");
  const [loadingSpinner, setLoadingSpinner] = useState(false);
  const { allMembres, loading, success, failure } = useSelector(
    (state) => state?.membre
  );
  const { user } = useSelector((state) => state?.auth);

  //ANCHOR: fix token of connected user

  useEffect(() => {
    dispatch(getAllMembres(staticToken));
  }, [dispatch, getAllMembres, user]);

  useEffect(() => {
    if (allMembres.length) {
      setListMembres(allMembres);
    }
  }, [allMembres]);

  const handleEdit = (membre) => {
    if (membre) history.push(`/updateMembre/${membre}`);
  };

  const DeleteModalBody = () => {
    return (
      <>
        <div className="modal-body">
          <p className="text-center">
            Êtes-vous sûr de vouloir supprimer ce Client ?
          </p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-primary m-0"
            onClick={() => handleRemove(idToDelete)}
          >
            {loadingSpinner && (
              <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            )}
            Confirmer
          </button>
        </div>
      </>
    );
  };

  const handleRemove = async (id) => {
    setLoadingSpinner(true);
    await dispatch(deleteMembre(id, staticToken));
    setLoadingSpinner(false);
    setDeleteModal(null);
  };

  return (
    <div className="background">
      <div className="header-user">
        <h3 className="titre-user">Gestion des Clients</h3>
        <Link to="/addMembre" className="add-user btn">
          {" "}
          Ajouter un Client
        </Link>
      </div>
      {loading && !listMembres && (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}

     

      <div className="card-tabel">
        <div className="table-responsive">
          <table className="table-user" width="70%">
            <thead className="thead-user">
              <tr>
                <td className="td-table">#</td>
                <td className="td-table">Nom</td>
                <td className="td-table">Prenom</td>
                <td className="td-table"> Adresse Email</td>
                <td className="td-table">Action</td>
              </tr>
            </thead>

            <tbody className="body-user">
              {!loading &&
                success &&
                listMembres?.map((membre, index) => {
                  return (
                    <tr key={membre._id}>
                      <td className="td-table">{index + 1}</td>
                      <td className="td-table">{membre.nom  || "-"}</td>
                      <td className="td-table">{membre.prenom  || "-"}</td>
                      <td className="td-table">{membre.email  || "-"}</td>
                      <td className="icon-membre">
                        <button
                          className="bg-transparent border-0"
                          onClick={() => handleEdit(membre._id)}
                        >
                          <i className="fas fa-edit edit"></i>
                        </button>
                        <button
                          className="bg-transparent border-0"
                          onClick={() => {
                            setIdToDelete(membre._id);
                            setDeleteModal(true);
                          }}
                        >
                          <i className="fas fa-trash-alt delete"></i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
      <CustomDialog
        show={deleteModal}
        handleClose={() => setDeleteModal(null)}
        header="Supprimer un client"
        body={<DeleteModalBody />}
      />
    </div>
  );
}

export default Membres;
