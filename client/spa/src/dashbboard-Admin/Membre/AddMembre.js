import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import "./Membres.css";
import { addMembre } from "../../redux/actions/membre";
import { isPassword, isEmail } from "../../helpers/utils";

function AddMembre() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [membre, setMembre] = useState({
    nom: "",
    prenom: "",
    email: "",
    password: "",
  });
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setErrorAdd("");
    setMembre({ ...membre, [name]: value });
  };


  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (!membre.nom || !membre.prenom || !membre.email || !membre.password) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        if (!isPassword(membre.password)) {
          setErrorAdd("Veuillez entrer 8 caracters au min !!");
          setLoadingAdd(false)
        }
        if (!isEmail(membre.email)) {
          setErrorAdd("Entrer un email valide !!");
          setLoadingAdd(false)
        }
        const newMembre = await dispatch(addMembre(membre, staticToken));
        if (newMembre) {
          setLoadingAdd(false);
          history.push("/membres");
          setErrorAdd("");
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue");
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="nom"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Email ..."
                name="email"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Password</label>
              <input
                type="password"
                class="form-control"
                placeholder="Password ..."
                name="password"
                onChange={handleChange}
              />
            </div>
            {errorAdd && (
              <div className="alert alert-danger" role="alert">
                {errorAdd}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto add-user"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Ajouter client
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddMembre;
