import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import "./Membres.css";
import { getMembre, updateMembre } from "../../redux/actions/membre";
import { isEmail } from "../../helpers/utils";

function UpdateMembre() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const { detailsMembre: details } = useSelector((state) => state.membre);

  const [membre, setMembre] = useState({});
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");

  const handleChange = (e) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("");
    setMembre({ ...membre, [name]: value });
  };
  useEffect(() => {
    dispatch(getMembre(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setMembre(details);
  }, [details]);

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (!membre.nom || !membre.prenom || !membre.email) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        if (!isEmail(membre.email)) {
          setErrorUpdate("Entrer un email valide !!");
          setLoadingUpdate(false);
        } else {
          const newMembre = await dispatch(
            updateMembre({ id, data: membre, token: staticToken })
          );
          if (newMembre) {
            setLoadingUpdate(false);
            history.push("/membres");
            setErrorUpdate("");
          }
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="nom"
                value={membre?.nom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                value={membre?.prenom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Email ..."
                name="email"
                value={membre?.email}
                onChange={handleChange}
              />
            </div>

            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update Membre
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateMembre;
