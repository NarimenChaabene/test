import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { staticToken } from "../../config";
import { useHistory, useParams } from "react-router";
import { isEmail } from "../../helpers/utils";
import "./Kinesitherapeute.css";
import { getKine, updateKine } from "../../redux/actions/kine";

function UpdateKine() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();

  const { detailsKine: details } = useSelector((state) => state.kine);

  const [kine, setKine] = useState({});
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const [errorUpdate, setErrorUpdate] = useState("");

  const handleChange = (e) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    setErrorUpdate("");
    setKine({ ...kine, [name]: value });
  };
  useEffect(() => {
    dispatch(getKine(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (details && Object.keys(details).length > 0) setKine(details);
  }, [details]);

  const update = async (e) => {
    e.preventDefault();
    setLoadingUpdate(true);
    if (
      !kine.nom ||
      !kine.prenom ||
      !kine.email ||
      !kine.numContact ||
      !kine.specialite
    ) {
      setErrorUpdate("* Veuillez remplir tous les champs !!");
      setLoadingUpdate(false);
    } else {
      try {
        if (!isEmail(kine.email)) {
          setErrorUpdate("Entrer un email valide !!");
          setLoadingUpdate(false);
        } else {
          const newKine = await dispatch(
            updateKine({ id, data: kine, token: staticToken })
          );
          if (newKine) {
            setLoadingUpdate(false);
            history.push("/kinesitherapeute");
            setErrorUpdate("");
          }
        }
      } catch (error) {
        setErrorUpdate("Erreur de survenue");
        setLoadingUpdate(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="nom"
                value={kine?.nom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                value={kine?.prenom}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Email ..."
                name="email"
                value={kine?.email}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Numero Contact</label>
              <input
                type="number"
                class="form-control"
                placeholder="Numero Contact ..."
                name="numContact"
                value={kine?.numContact}
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Specialite</label>
              <input
                type="text"
                class="form-control"
                placeholder="Specialite ..."
                name="specialite"
                value={kine?.specialite}
                onChange={handleChange}
              />
            </div>
            {errorUpdate && (
              <div className="alert alert-danger" role="alert">
                {errorUpdate}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={update}
              >
                {loadingUpdate && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Update Kine
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateKine;
