import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import CustomDialog from "../../components/dialog/CustomDialog";
import { staticToken } from "../../config";
import { deleteKine, getAllKines } from "../../redux/actions/kine";
import "./Kinesitherapeute.css";

function Kinesitherapeute() {
  const dispatch = useDispatch();
  const history = useHistory();

  const [listKines, setListKines] = useState([]);
  const [deleteModal, setDeleteModal] = useState(false);
  const [idToDelete, setIdToDelete] = useState("");
  const [loadingSpinner, setLoadingSpinner] = useState(false);
  const { allKines, loading, success, failure } = useSelector(
    (state) => state?.kine
  );
  const { user } = useSelector((state) => state?.auth);

  //ANCHOR: fix token of connected user

  useEffect(() => {
    dispatch(getAllKines(staticToken));
  }, [dispatch, getAllKines, user]);

  useEffect(() => {
    if (allKines?.length) {
      setListKines(allKines);
    }
  }, [allKines]);

  const handleEdit = (kine) => {
    if (kine) history.push(`/updateKine/${kine}`);
  };

  const DeleteModalBody = () => {
    return (
      <>
        <div className="modal-body">
          <p className="text-center">
            Êtes-vous sûr de vouloir supprimer ce Kine ?
          </p>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-primary m-0"
            onClick={() => handleRemove(idToDelete)}
          >
            {loadingSpinner && (
              <div class="spinner-border spinner-border-sm" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            )}
            Confirmer
          </button>
        </div>
      </>
    );
  };

  const handleRemove = async (id) => {
    setLoadingSpinner(true);
    await dispatch(deleteKine(id, staticToken));
    setLoadingSpinner(false);
    setDeleteModal(null);
  };

  return (
    <div>
      <div className="header-kine">
        <h3 className="titre-kine">Gestion des Kinésithérapeute</h3>
        <Link to="/addKine" className="add-kine btn">
          {" "}
          Ajouter un Kine
        </Link>
      </div>

      {loading && !listKines && (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}

      <div className="card-tabel">
        <div className="table-responsive">
          <table className="table-kine" width="70%">
            <thead className="thead-kine">
              <tr>
                <td className="td-kine">#</td>
                <td className="td-kine">Nom</td>
                <td className="td-kine">Prenom</td>
                <td className="td-kine"> Numero contact</td>
                <td className="td-kine"> Adresse Email</td>
                <td className="td-kine">Action</td>
              </tr>
            </thead>

            <tbody className="tbody-kiney">
              {!loading &&
                success &&
                listKines?.map((kine, index) => {
                  return (
                    <tr>
                      <td className="td-kine">{index + 1}</td>
                      <td className="td-kine">{kine.nom}</td>
                      <td className="td-kine">{kine.prenom}</td>
                      <td className="td-kine">{kine.numContact}</td>
                      <td className="td-kine">{kine.email}</td>
                      <td className="icon-kine">
                        <button
                          className="bg-transparent border-0"
                          onClick={() => handleEdit(kine._id)}
                        >
                          <i className="fas fa-edit edit"></i>
                        </button>
                        <button
                          className="bg-transparent border-0"
                          onClick={() => {
                            setIdToDelete(kine._id);
                            setDeleteModal(true);
                          }}
                        >
                          <i className="fas fa-trash-alt delete"></i>
                        </button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
      <CustomDialog
        show={deleteModal}
        handleClose={() => setDeleteModal(null)}
        header="Supprimer un Kinesitherapeute"
        body={<DeleteModalBody />}
      />
    </div>
  );
}

export default Kinesitherapeute;
