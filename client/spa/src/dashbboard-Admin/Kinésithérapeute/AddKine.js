import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { staticToken } from "../../config";
import { useHistory } from "react-router";
import { isPassword, isEmail } from "../../helpers/utils";
import "./Kinesitherapeute.css";
import { addKine } from "../../redux/actions/kine";

function AddKine() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [kine, setKine] = useState({
    nom: "",
    prenom: "",
    email: "",
    password: "",
    numContact: "",
    specialite: "",
  });
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [errorAdd, setErrorAdd] = useState("");

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setErrorAdd("");
    setKine({ ...kine, [name]: value });
  };

  const add = async (e) => {
    e.preventDefault();
    setLoadingAdd(true);
    if (
      !kine.nom ||
      !kine.prenom ||
      !kine.email ||
      !kine.password ||
      !kine.numContact ||
      !kine.specialite
    ) {
      setErrorAdd("* Veuillez remplir tous les champs !!");
      setLoadingAdd(false);
    } else {
      try {
        if (!isPassword(kine.password)) {
          setErrorAdd("Veuillez entrer 8 caracters au min !!");
          setLoadingAdd(false);
        }
        if (!isEmail(kine.email)) {
          setErrorAdd("Entrer un email valide !!");
          setLoadingAdd(false);
        }
        const newKine = await dispatch(addKine(kine, staticToken));
        if (newKine) {
          setLoadingAdd(false);
          history.push("/kinesitherapeute");
          setErrorAdd("");
        }
      } catch (error) {
        setErrorAdd("Erreur de survenue");
        setLoadingAdd(false);
      }
    }
  };

  return (
    <>
      <div class="container my-center ">
        <div class="row h-50 justify-content-center align-items-center">
          <form class="col-6">
            <div class="form-group my-3">
              <label className="py-2">Nom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Nom ..."
                name="nom"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Prenom</label>
              <input
                type="text"
                class="form-control"
                placeholder="Prenom ..."
                name="prenom"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Email</label>
              <input
                type="email"
                class="form-control"
                placeholder="Email ..."
                name="email"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Password</label>
              <input
                type="password"
                class="form-control"
                placeholder="Password ..."
                name="password"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Numero Contact</label>
              <input
                type="number"
                class="form-control"
                placeholder="Numero Contact ..."
                name="numContact"
                onChange={handleChange}
              />
            </div>
            <div class="form-group my-3">
              <label className="py-2">Specialite</label>
              <input
                type="text"
                class="form-control"
                placeholder="Specialite ..."
                name="specialite"
                onChange={handleChange}
              />
            </div>
            {errorAdd && (
              <div className="alert alert-danger" role="alert">
                {errorAdd}
              </div>
            )}
            <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
              <button
                className="btn btn-primary mx-auto my-auto"
                type="submit"
                onClick={add}
              >
                {loadingAdd && (
                  <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                Add Kine
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddKine;
