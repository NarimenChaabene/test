var express = require('express');
var router = express.Router();
const Seances = require('../../models/Seances');
const verify = require('../Authentication/verifyToken');

var bodyParser = require('body-parser');




router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Events 's routes
router.get('/',  async (req, res) => {
   
    try {
         var seanceClient = await Seances.find({})
         res.send(seanceClient);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Seances.findById(req.params.id)
    .then((seanceClient) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(seanceClient);
    }, (err) => next(err))
    .catch((err) => next(err));
})

router.post('/', async (req,res) => {
    const seances = new Seances ({
       Description: req.body.Description,
        prix : req.body.prix,
       dateDebut : req.body.dateDebut,
       dateFin : req.body.dateFin, 
       nbr_Place_Max: req.body.nbr_Place_Max,
       nbr_Reservation:req.body.nbr_Reservation

      

     
  });
  
  
  try{
      const savedSeance = await Seances.save(); 
      res.send(savedSeance);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

  



//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
    Seances.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('seances removed');
    }
    else { console.log('Failed to Delete seance Details: ' + err); }
    });
    });


    //Router Controller for PUT request

    router.route("/:id").put((req, res, next) => {
        Seances.findByIdAndUpdate(
          req.params.id,
          {
            $set: req.body,
          },
          (error, data) => {
            if (error) {
              return next(error);
            } else {
              
              console.log("seance updated !");
              res.send(req.body);
            }
          }
        );
      });

module.exports = router;




