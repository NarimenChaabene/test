var express = require('express');
var router = express.Router();
const Client = require('../../models/Client');
const verify = require('../Authentication/verifyToken');



router.get('/', async (req, res) => {
   
    try {
         var client = await Client.find({})
         res.send(client);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Client.findById(req.params.id)
    .then((client) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(client);
    }, (err) => next(err))
    .catch((err) => next(err));
})

router.post('/', async (req,res) => {
    const client = new Client ({
      
     nom : req.body.nom,
     prenom : req.body.prenom,
     email : req.body.email,
     password : req.body.password,
     Phone : req.body.Phone,
      

     
  });
  
  
  try{
      const savedClient = await client.save(); 
      res.send(savedClient);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
    Client.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('client removed');
    }
    else { console.log('Failed to Delete client Details: ' + err); }
    });
    });

//Router controller for update
router.route("/:id").put((req, res, next) => {
    Client.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          
          console.log("client updated !");
          res.send(req.body);
        }
      }
    );
  });


  

  module.exports = router;