var express = require('express');
var router = express.Router();
const Reservation = require('../../models/Reservation');
const verify = require('../Authentication/verifyToken');


var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Events 's routes
router.get('/', async (req, res) => {
   
    try {
         var reservationClient = await Reservation.find({})
         res.send(reservationClient);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Reservation.findById(req.params.id)
    .then((reservationClient) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(reservationClient);
    }, (err) => next(err))
    .catch((err) => next(err));
})

router.post('/' , async (req,res) => {
    const reservationClient = new Reservation ({
      Description: req.body.Description,
      typeReservation: req.body.typeReservation,
      dateReservation: req.body.dateReservation, 
      montant : req.body.montant,
      datePaiement : req.body.datePaiement,  
  });
  
  
  try{
      const savedReservation = await reservationClient.save(); 
      res.send(savedReservation);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

  

//Router Controller for DELETE request
router.delete('/:id', verify,(req, res) => {
    Reservation.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('reservation removed');
    }
    else { console.log('Failed to Delete reservation Details: ' + err); }
    });
    });

//Router controller for update
router.route("/:id").put((req, res, next) => {
    Reservation.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data);
          console.log("Reservation unsubscfibed !");
        }
      }
    );
  });

module.exports = router;




