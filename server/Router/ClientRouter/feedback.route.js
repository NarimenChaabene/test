var express = require('express');
var router = express.Router();
const Feedback = require('../../models/Feedback');
const verify = require('../Authentication/verifyToken');





router.post('/', async (req,res) => {
    const feedback = new Feedback ({
      
     nom : req.body.nom,
     prenom : req.body.prenom,
     message : req.body.message,
     rating : req.body.rating,
     date : req.body.date,
      

     
  });
  
  
  try{
      const savedFeedback = await feedback.save(); 
      res.send(savedFeedback);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
    Feedback.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('feedback removed');
    }
    else { console.log('Failed to Delete feedback Details: ' + err); }
    });
    });

//Router controller for update
router.route("/:id").put((req, res, next) => {
    Feedback.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          
          console.log("feedback updated !");
          res.send(req.body);
        }
      }
    );
  });


  

  module.exports = router;