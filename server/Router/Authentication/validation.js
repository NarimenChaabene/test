
const Joi = require('@hapi/joi');
const { Mongoose } = require('mongoose');

const registerValidation = data =>{
    const Schema = Joi.object({
        nom: Joi.string().min(5).required(),
        prenom: Joi.string().min(5).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(7).required(),
        role: Joi.string().min(4).required()
    });  

    return Schema.validate(data);
};

const loginValidation =data =>{
    const Schema = Joi.object({
        
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(7).required()
    });  

    return Schema.validate(data);
};

const prospectValidation = data => {
    const Schema = Joi.object({
        nom: Joi.string().min(5).required(),
        prenom: Joi.string().min(5).required(),
        email: Joi.string().min(6).required().email(),
        message: Joi.string().min(7).required(),
        telephone: Joi.string().min(8).required()

    })
    return Schema.validate(data);
}

module.exports.registerValidation =registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.prospectValidation = prospectValidation;