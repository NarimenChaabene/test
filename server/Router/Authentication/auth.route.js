const router = require('express').Router();

//import sendEmail from '../../email/email';
const User = require('../../models/User');
var bodyParser = require('body-parser');
const {registerValidation, loginValidation} = require('./validation');
const bcrypt =require('bcryptjs');
const jwt = require('jsonwebtoken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());



//Register
router.post('/register', async (req,res) => {


//validate data before create user
const {error} = registerValidation(req.body);
if(error) 
   return res.status(400).send(error.details[0].message);

// checking if the user is already in the database
const emailExist = await User.findOne({email: req.body.email});
if (emailExist)
   return res.status(400).send('Email already exists');


   
// Hash passwords
const salt =  await bcrypt.genSalt(10);
const hashedPassword = await bcrypt.hash(req.body.password, salt);
// Create a new user
const user= new User ({
    email: req.body.email,
    password: hashedPassword,
    nom: req.body.nom,
    prenom: req.body.prenom,
    role: req.body.role
    
    
});
    try{
        const savedUser = await user.save(); 
        res.send(savedUser);
       }
        
    catch (err){
        res.status(400).send(err);
    }
  
    });
 
    //LOGIN
router.post('/login', async (req, res) =>{

         
     //validate data before create user
const {error} = loginValidation(req.body);
if(error) 
   return res.status(400).send(error.details[0].message);


    // checking if the email exists
         const emailExist = await User.findOne({email: req.body.email});
         if (!emailExist)
            return res.status(400).send("Email is not found"); 
   
            /*Password is correct
 
        const validPass = await bcrypt.compare(req.body.password, user.password);
        if(!validPass)
           return res.status(400).send("password is incorrect") */
    



// create and assign a token
const token = jwt.sign({_id : User._id}, process.env.TOKEN_SECRET);
res.header('auth-token', token).send(token);
//res.send('logged in')

          
});



//reset password


/*const auth = {
async sendResetLink(req, res, next) {
    try {
      const { email } = req.body;
      const user = await User.findOne({ where: { email } });
      if (!email) {
        return res.status(400).send({ error: 'Email is required' });
      }
      if (!validator.isEmail(email)) {
        return res.status(400).send({ error: 'Invalid email' });
      }
      if (!user) {
        return res.status(404).send({ error: 'User not found' });
      }
      const token = jwtToken.createToken(user);
      const link = `${req.protocol}://localhost:4000/reset_password/${token}`;
      await sendEmail(
        email,
        'noreply@todo.com',
        'Best To Do password reset',
        `
        <div>Click the link below to reset your password</div><br/>
        <div>${link}</div>
        `
      );
      return res.status(200).send({ message: 'Password reset link has been successfully sent to your inbox' });
    } catch (e) {
      return next(new Error(e));
    }
  },

  async resetPassword(req, res, next) {
    try {
      const { password } = req.body;
      const { token } = req.params;
      const decoded = jwtToken.verifyToken(token);
      const hash = hashPassword(password);
      const updatedUser = await User.update(
        { password: hash },
        {
          where: { id: decoded.userId },
          returning: true,
          plain: true,
        }
      );
      const { id, name, email } = updatedUser[1];
      return res.status(200).send({ token, user: { id, name, email } });
    } catch (e) {
      return next(new Error(e));
    }
  }
};*/

    module.exports = router;
