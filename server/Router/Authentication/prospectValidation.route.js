const router = require ('express').Router()
const Prospect = require ('../../models/Prospect')
const {prospectValidation} = require ('./validation') 

const bcrypt =require('bcryptjs');


router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


//pré-inscription

router.post ('./preinscription' , async(req,res) =>{

    //validate data before create prospect user
    const {error} = prospectValidation(req.body)
    if(error) 
   return res.status(400).send(error.details[0].message);

   //create prospect
   const prospect= new Prospect ({
    
    nom: req.body.nom,
    prenom: req.body.prenom,
    email: req.body.email,
    message: req.body.message,
    telephone: req.body.telephone
    
    
});
    try{
        const saveProspect = await prospect.save(); 
        res.send(saveProspect);
       }
        
    catch (err){
        res.status(400).send(err);
    }
  
    });


module.exports = router;