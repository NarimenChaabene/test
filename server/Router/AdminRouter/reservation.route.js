var express = require('express');
var router = express.Router();
const Reservation = require('../../models/Reservation');
const verify = require('../Authentication/verifyToken');

var bodyParser = require('body-parser');



router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Events 's routes
router.get('/', async (req, res) => {
   
    try {
         var reservation = await Reservation.find({})
         res.send(reservation);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Reservation.findById(req.params.id)
    .then((reservation) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(reservation);
    }, (err) => next(err))
    .catch((err) => next(err));
})

router.post('/',  async (req,res) => {
    const reservation = new Reservation ({
        Description: req.body.Description,
        Heure: req.body.Heure,
        dateReservation: req.body.dateReservation, 
        etat: req.body.etat,
       
  });
  
  
  try{
      const savedreservation = await reservation.save(); 
      res.send(savedreservation);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
    Reservation.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('reservation removed');
    }
    else { console.log('Failed to Delete reservation Details: ' + err); }
    });
    });


    //update
    router.route("/:id").put((req, res, next) => {
        Reservation.findByIdAndUpdate(
          req.params.id,
          {
            $set: req.body,
          },
          (error, data) => {
            if (error) {
              return next(error);
            } else {
              res.json(data);
              console.log("Reservation unsubscfibed !");
            }
          }
        );
      });



module.exports = router;




