var express = require('express');
var router = express.Router();
const verify = require('../Authentication/verifyToken');


var bodyParser = require('body-parser');
const Client = require('../../models/Client');


router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


//
router.get('/', async (req, res) => {
   
  try {
       var client = await Client.find({})
       res.send(client);
  }

  catch (err) {
       res.status(500).send();
   }
});

router.route('/:id')
.get((req,res,next) => {
  Client.findById(req.params.id)
  .then((client) => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');    
      res.json(client);
  }, (err) => next(err))
  .catch((err) => next(err));
})

router.post('/', async (req,res) => {
    const client = new Client ({
        nom: req.body.nom,
        prenom: req.body.prenom,
        email : req.body.email,
        password: req.body.password,
        
       

      

     
  });
  
  
  try{
      const savedClient = await client.save(); 
      res.send(savedClient);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//delete

  router.delete('/:id', (req, res) => {
    Client.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('cclient removed');
    }
    else { console.log('Failed to Delete client Details: ' + err); }
    });
    });

    //update

    router.route("/:id").put((req, res, next) => {
        Client.findByIdAndUpdate(
          req.params.id,
          {
            $set: req.body,
          },
          (error, data) => {
            if (error) {
              return next(error);
            } else {
              res.json(data);
              console.log("client unsubscfibed !");
            }
          }
        );
      });
module.exports = router;




