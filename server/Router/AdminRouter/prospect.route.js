var express = require('express')
var router = express.Router()
const Prospect = require ('../../models/Prospect')
var bodyParser = require('body-parser')


router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

// Get prospect
router.get('/', async (req, res) => {
   
    try {
         var prospect = await Prospect.find({})
         res.send(prospect);
    }

    catch (err) {
         res.status(500).send();
     }
 });


 router.route('/:id')
 .get((req,res,next) => {
     Prospect.findById(req.params.id)
     .then((prospect) => {
         res.statusCode = 200;
         res.setHeader('Content-Type', 'application/json');    
         res.json(prospect);
     }, (err) => next(err))
     .catch((err) => next(err));
 })


 //post

 router.post('/', async (req,res) => {
    const prospect = new Prospect ({
        nom: req.body.nom,
        prenom: req.body.prenom,
        telephone: req.body.telephone,
        email : req.body.email,
        Message: req.body.Message,
       

      

     
  });
  
  
  try{
      const savedProspect = await prospect.save(); 
      res.send(savedProspect);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });


 //Router Controller for DELETE request
router.delete('/:id',(req, res) => {
    Prospect.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('prospect removed');
    }
    else { console.log('Failed to Delete prospect Details: ' + err); }
    });
    });


//Router controller for update
router.route("/:id").put((req, res, next) => {
    Prospect.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data);
          console.log("Prospect unsubscfibed !");
        }
      }
    );
  });


    module.exports = router;