var express = require('express');
var router = express.Router();

//const verify = require('../Authentication/verifyToken');

var bodyParser = require('body-parser');
const Visiteurs = require('../../models/Visiteurs');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', async (req, res) => {
   
    try {
         var visiteur = await Visiteurs.find({})
         res.send(visiteur);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Visiteur.findById(req.params.id)
    .then((visiteur) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(visiteur);
    }, (err) => next(err))
    .catch((err) => next(err));
})




module.exports = router;

