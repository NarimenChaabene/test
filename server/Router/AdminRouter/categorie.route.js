var express = require('express');
var router = express.Router();
const Categorie = require('../../models/Categorie');
const verify = require('../Authentication/verifyToken');
const checkRole =require('../../middlewares/checkRole')

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Category routes
router.get('/', async (req, res) => {
   
    try {
         var categorie = await Categorie.find({})
         res.send(categorie);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Categorie.findById(req.params.id)
    .then((categorie) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(categorie);
    }, (err) => next(err))
    .catch((err) => next(err));
})


router.post('/', async (req,res) => {
    const categorie = new Categorie ({
      
      Description: req.body.Description,
      Titre: req.body.Titre,
      

     
  });
  
  
  try{
      const savedCategorie = await categorie.save(); 
      res.send(savedCategorie);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//Router Controller for DELETE request
router.delete('/:id',(req, res) => {
    Categorie.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('categorie removed');
    }
    else { console.log('Failed to Delete categorie Details: ' + err); }
    });
    });

//Router controller for update
router.route("/:id").put((req, res, next) => {
    Categorie.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          
          console.log("Categorie updated !");
          res.send(req.body);
        }
      }
    );
  });

module.exports = router;




