var express = require('express');
var router = express.Router();
const Kinesitherapeute = require('../../models/Kinesitherapeute');
const verify = require('../Authentication/verifyToken');
const checkRole = require('../../middlewares/checkRole')

var bodyParser = require('body-parser');



router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Events 's routes
router.get('/',  async (req, res) => {
   
    try {
         var kinesitherapeute = await Kinesitherapeute.find({})
         res.send(kinesitherapeute);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Kinesitherapeute.findById(req.params.id)
    .then((Kinesitherapeute) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(Kinesitherapeute);
    }, (err) => next(err))
    .catch((err) => next(err));
})


router.post('/',async (req,res) => {
    const kinesitherapeute = new Kinesitherapeute ({
        nom: req.body.nom,
        prenom: req.body.prenom,
        numContact: req.body.numContact,
        email : req.body.email,
        password: req.body.password,
        specialite : req.body.specialite
     
  });
  
  
  try{
      const savedkine = await  kinesitherapeute.save(); 
      res.send(savedkine);
     }
      
  catch (err){
      res.status(400).send(err);
  }
  });

//Router Controller for DELETE request
router.delete('/:id', (req, res) => {
    Kinesitherapeute.findByIdAndRemove(req.params.id, (err) => {
    if (!err) {
    res.send('kinesitherapeute removed');
    }
    else { console.log('Failed to Delete kinesitherapeute Details: ' + err); }
    });
    });

//Router controller for update
router.route("/:id").put((req, res, next) => {
    Kinesitherapeute.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data);
          console.log("kinesitherapeute unsubscfibed !");
        }
      }
    );
  });

module.exports = router;




