var express = require('express');
var router = express.Router();
const Feedback = require('../../models/Feedback');
const verify = require('../Authentication/verifyToken');

var bodyParser = require('body-parser');


router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', async (req, res) => {
   
    try {
         var feedback = await Feedback.find({})
         res.send(feedback);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    Feedback.findById(req.params.id)
    .then((feedback) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(feedback);
    }, (err) => next(err))
    .catch((err) => next(err));
})



module.exports = router;