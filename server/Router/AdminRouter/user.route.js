var express = require('express');
var router = express.Router();

//const verify = require('../Authentication/verifyToken');

var bodyParser = require('body-parser');
const User = require('../../models/User');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', async (req, res) => {
   
    try {
         var user = await User.find({})
         res.send(user);
    }

    catch (err) {
         res.status(500).send();
     }
 });

 router.route('/:id')
.get((req,res,next) => {
    User.findById(req.params.id)
    .then((user) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');    
        res.json(user);
    }, (err) => next(err))
    .catch((err) => next(err));
})

module.exports = router;