const mongoose =require('mongoose');
var express = require('express');
var router = express.Router();
const Schema = mongoose.Schema;
const feedbackSchema = new  mongoose.Schema({

   nom: {
       type: String,
      
   },
   prenom: {
       type: String,
       
      
   }, 

   message: {
    type: String,
    required: true,
   
}, 

   rating: {
    type: Number,
    required: true,
   
},

   date: {
    type: Date,
    default: Date.now(),
   
   
}


   
});
/*let rating =req.body.rating
  const message= req.body.message

  if (rating) {
    rating = parseInt(rating)
  }

  if (!rating || rating < 1 || rating > 5) {
    return res.status(200).json({
      success: false,
      Message: 'Rating must be between 1 and 5.'
    })
  }*/

module.exports = mongoose.model('feedback', feedbackSchema);