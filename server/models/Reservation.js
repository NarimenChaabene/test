const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reservationSchema = new Schema ({
    Description : {
        type : String,
        required:true
    },

    Heure : {
        type : Date,
        required:true
    },

    dateReservation : {
        type : Date,
        required:true
    },

    etat : {
        type : String,
        required:true
    }

})

module.exports = mongoose.model ('reservation', reservationSchema);