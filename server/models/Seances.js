const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const seanceSchema = new Schema ({
    Description : {
        type : String,
        required:true
    },

    prix : {
        type : Number,
        required:true
    },

    dateDebut : {
        type : Date,
        required:true
    },

    dateFin : {
        type :Date,
        required:true
    },

    nbr_Place_Max : {
        type :Number,
        required:true
    },

    nbr_Reservation : {
        type :Number,
        required:true
    },

})

module.exports = mongoose.model ('seance', seanceSchema);