
const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
  nom: {
    type: String,
    required: [true, 'Please tell us your name!']
  },

  prenom:{
    type: String,
    required: [true, 'Please tell us your prenom!']
  },

  email: {
    type: String,
    required: [true, 'Please provide your email'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Please provide a valid email']
  },

  role: {
    type: String,
    enum: ['client', 'admin' , 'kinesitherapeute'],
    default: 'client'
  },

  password: {
    type: String,
    required: [true, 'merci de saisir votre mots de passe'],
    minlength: 8,
    select: false
    // validate: [validator.is, 'Please provide a valid email']
  },
 
})
module.exports = User = mongoose.model('user', userSchema);

