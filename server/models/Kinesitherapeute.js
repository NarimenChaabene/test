const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const kinesitherapeuteSchema = new Schema ({
    nom : {
        type: String,
        required:true
        
    },

    prenom : {
        type: String,
        required:true
        
    },

  
    email : {
        type: String,
        required:true
        
    },

    password : {
        type: String,
        required:true
        
    },
    specialite : {
        type: String,
        required:true
        
    },

   numContact : {
        type: String,
        required:true
        
    }
})

module.exports = mongoose.model('kinesitherapeute',kinesitherapeuteSchema );

//module.exports = mongoose.model('client', kinesitherapeuteSchema);