 const express = require('express');
 const cors = require('cors')
 var nodemailer = require('nodemailer');
 const app = express();


 
 require('dotenv/config');
var db = require('./db/db');
var bodyParser = require('body-parser');

//import route
const authRoute = require('./Router/Authentication/auth.route');

 

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json
app.use(bodyParser.json());

app.use(express.json());
app.use(cors())



//Route Middleware

app.use('/api/user', authRoute);


//Import authentication's route
 
const registerRoute = require('./Router/Authentication/auth.route.js');
app.use('/api/user', registerRoute);


//Administrator' routes 
const categorieRoute = require('./Router/AdminRouter/categorie.route');
app.use('/categorie', categorieRoute);

const clientRoute = require('./Router/AdminRouter/client.route');
app.use('/client', clientRoute);

const kineRoute = require('./Router/AdminRouter/kine.route');
app.use('/kinesitherapeute', kineRoute);

const reservationRoute = require('./Router/AdminRouter/reservation.route');
app.use('/reservation', reservationRoute);

const seanceRoute = require('./Router/AdminRouter/seance.route');
app.use('/seance', seanceRoute);

const feedbackRoute = require('./Router/AdminRouter/feedback.route');
app.use('/feedback', feedbackRoute);


const userRoute = require('./Router/AdminRouter/user.route');
app.use('/api/user',userRoute);

const prospectRouter = require ('./Router/AdminRouter/prospect.route')
app.use('/prospect',prospectRouter )




// Client router
const reservationsClientRouter = require('./Router/ClientRouter/reservations.route');
app.use('/reservationClient', reservationsClientRouter);

const seancesClientRouter = require('./Router/ClientRouter/seances.route');
app.use('/seanceClient', seancesClientRouter);

const feedbackClientRouter = require('./Router/ClientRouter/feedback.route');
app.use('/feedbackClient', feedbackClientRouter);

const EditProfileClientRouter = require('./Router/ClientRouter/client.route');
app.use('/editProfileClient', EditProfileClientRouter);



//email 
const sendMail=require('./email/email')
app.use('/email',sendMail)

const sendemail=require('./email/emailSG')
app.use('/sendemail',sendemail)

//chat 
const http = require('http').createServer(app)
app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

// Socket 
const io = require('socket.io')(http)

io.on('connection', (socket) => {
    console.log('User Connected...')
    socket.on('message', (msg) => {
        socket.broadcast.emit('message', msg)
    })
    socket.on("disconnect", () => {
        console.log("User Disconnected");
      });

})




//app.listen(4000, () => console.log ('Server up and running'));
const port = 4000;
http.listen(port, function(){
    console.log("Server running on localhost", +port);
});

//test sendgrid
const path = require("path");
const sendEmail = require("./email/emailSG");
app.use(express.urlencoded({ extended: false }));
app.use("/public", express.static(path.join(__dirname, "public")));

app.set("view engine", "ejs");

app.get("/sendemail", (req, res) => {
  res.render("contact");
});

app.get("/sent", (req, res) => {
  res.render("sent");
});

app.post("/sendemail", (req, res) => {
    
  const { Nom , Prénom , To , From } = req.body;

  const from = "narimenchaabene@gmail.com";
  const to = "bahloulhind4@gmail.com";

  const subject = "New Contact Request";

  const output = `
    <p>You have a new Contact Request</p>
    <h3>Contact Details</h3>
    <ul>
      <li>Name: ${Nom}</li>
      <li>Surname: ${Prénom}</li>
      <li>Email: ${To}</li>
      <li>Email: ${From}</li>
    </ul>
  `;

  sendEmail(to, from, subject, output);
  res.redirect("/sent");

  
});







